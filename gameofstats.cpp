/* STATS
MIGHT NOT BE ACCURATE

HUMAN       ELF         DWARF       ORC         GIANT       DEMON       ANGEL       DILDAUS     ALPACA
+1 INTE     +2 DEXT     +2 INTE     -3 INTE     -1 LUCK     +3 LUCK     -3 LUCK     +2 VITA     -4 STRE
+1 VITA     +1 INTE     +1 STRE     +2 STRE     -3 INTE     +1 STRE     +2 ENDU     +2 ENDU     -2 INTE
+1 DEXT     -1 STRE     +1 ENDU     +2 ENDU     +3 VITA     -2 VITA     +2 VITA     -3 INTE     -3 DEXT
-------     -------     -------     -------     +1 ENDU     -1 DEXT     +1 DEXT     +2 LUCK     -------

100 HP      100 HP      100 HP      120 HP      120 HP      90  HP      110 HP      100 HP      250 HP
20  MP      40  MP      0   MP      0   MP      0   MP      20  MP      20  MP      10  MP      0   MP

DMG MAX 5   DMG MAX 5   DMG MAX 4   DMG MAX 5   DMG MAX 6   DMG MAX 4   DMG MAX 5   DMG MAX 3   DMG MAX 2
DMG MIN 1   DMG MIN 1   DMG MIN 2   DMG MIN 1   DMG MIN 2   DMG MIN 2   DMG MIN 1   DMG MIN 1   DMG MIN 1

+5% XP      +10% DODGE  +CRAFT      +BERSEKER   +ENDURANCE  +DEMON PACT +ANGEL AID  +PIERCE     +SPIT
+10% GOLD   +5% MAGIC.A +5% REGEN   +RAGE       +5% CRIT    +THORNS     +HOLY AURA  +PLEASURE   +CLOUD

*/
/* FINALS
MIGHT NOT BE ACCURATE

FINAL DMG = (DMG MAX+DMG MIN)/2+(DMG MIN*STRE)/2+(DMG MIN*LUCK)/LUCK
FINAL DEF = (STRE+ENDU)/2
DODGE     = (DEXT*LUCK)/2

*/
/* PLANS
MIGHT NOT BE ACCURATE

*GAME OF STATS
    +BOSSES
    +ENEMYS
    +RACE
    +STATS
    +MAPS
    +ITEMS
    +TITLES

*LAMA RAINBOW DLC
    +3 BOSSES
        +FILIPS MOM
        +RAINBOW LLAMA
        +FAT JELLY
    +10 ENEMYS
        +NANO LLAMA
        +JELLY BEAN
        +FLOATING HAMMER
        +ERROR 404
        +PINK LLAMA
        +HOLY LLAMA
        +50'POUND
        +HYDRA LLAMA
        +LEGLESS LLAMA
        +PILIF
    +2 RACE
        +DILDAUS    //PINK,VIBRATING,LONG
        +ALPACA     //THREE HEADED ALPACA,NINJA ALPACA,WILD ALPACA
    +1 STATS
        +FUN        //DAMAGE BOOST
    +1 MAP
        +JELLY BUTTER UNICORN LAND      //LVL 50+

*ANGELS & DEMONS DLC
    +4 BOSSES
        +SATAN
        +GOD
        +LUCIFER
        +JESSUS
    +5 ENEMYS
        +DEVIL
        +ANGEL
        +DEMON
        +HOLY SPHERE
        +REAPER
    +2 RACE
        +ANGELS
        +DEMONS
    +2 STATS
        +FAITH
        +KARMA
    +3 MAP
        +HEAVEN
        +HELL
        +THE BORDER
*/
/* COPYRIGHT
ALL THE COPYRIGHTS OF THIS
PROGRAM GO TO CRACK TEAM
AND ALL OF ITS MEMBERS.

ANY ILLEGAL COPY OF THIS
PROGRAM WILL BE PUNISHED
BY THE LAW.

TRADEMARK CRACKTEAM
*/
/* UPDATE LOG


28/02/2013 - INDEV
    +PROJECT "GAME OF STATS" STARTED

05/03/2013 - INDEV
    +DLC IDEAS STARTED
    +RAINBOW LAMA DLC PLANS

09/03/2013 - INDEV
    +STARTED UPDATE LOG
    +TUTORIAL
    +STORY
    +ANGELS & DEAMONS DLC PLANS

10/03/2013 - INDEV
    +STORY UPDATE
    +STORY MENU BORDER
    +CAN NOT SKIP TUTORIAL
    +NEW TUTORIAL
    +RACE MENU

11/03/2013 - INDEV
    +NEW RACE MENU

12/03/2013 - INDEV
    +GENDER MENU

13/03/2013 - INDEV
    +NEW INPUT (GETCH())
    +REAL GAME PLANS
    +SECURITY LOGIN
    +MERCENARY HUMAN CLASS

14/03/2013 - INDEV
    +REMOVED SECURITY LOGIN
    +NAPOLEON EASTER EGG
    +NEW MENU BORDERS
    +HUMAN CLASS MENU
    +HUMAN STORY
    +ELF STORY
    +DWARF STORY
    +ORC STORY
    +REACHED 1000 LINES OF CODE

15/03/2013 - INDEV
    +GIANT STORY

16/03/2013 - INDEV
    +ELF CLASS MENU
    +DWARF CLASS MENU
    +NOTE 'GETTING CLOSE TO THE ALPHA RELEASE'

18/03/2013 - INDEV
    +ORC CLASS MENU
    +GIANT CLASS MENU
    +DEBUG OPTION IN MAIN MENU
    +BUG LOG , COMMENT BLOCK
    +REVIEW STATS AFTER CHAR CREATION
    +NOTE 'JUST A MAP AWAY FROM THE ALPHA RELEASE, I AM SO EXITED =D'
    +EACH MENU HAS A SPECIFIC TITLE SHOWING THE NAME OF TE GAME, THE MENU AND THE VERSION
    +REACHED 1500 LINES OF CODE
    +TO DO LIST , COMMENT BLOCK

21/03/2013 - INDEV
    +SORTING MENUS USING {}
    +MAP FRAME READY
    +MAP LOCATIONS
    +REMOVED TO DO LIST BLOCK
    +REMOVED CHAT BLOCK
    +HUMAN LAND MAP
    +HUMAN MAP LOCATIONS
    +RANDOM GENERATOR
    +RANDOM TEXT ON LOADING SCREEN
    +LAMA DLC EASTER EGG IN THE LOADING SCREEN
    +MAX HP (INT)
    +LOADING TIME CHANGED FROM 500ms TO 600ms
    +COLOR , COMMENT BLOCK
    +NEW COLOR OPTIONS
    +NEW COLOR OPTIOSN ARE BUGGY AS F***
    +REACHED 2000 LINES OF CODE
    +NOTE 'COMPILING TIME HAS INCREASED'
    +COLOR OPTIONS ARE NOW STABLE
    +NOTE 'GAME HAS 1MB'
    +EACH PART HAS A TAG   COMPLETE/WIP

22/03/2013 - INDEV
    +NEW MAP TEMPLATE , COMMENT BLOCK
    +112 MAPS FOR HUMAN KINGDOM
    +REACHED 5500 LINES OF CODE
    +ADDED 50% OF THE MAP X,Y
    +FINISHED THE X,Y
    +FINISHED ADDING THE @(PLAYER)(HUMAN)
    +FINISHED ADDING THE X(LOCATIONS)HUMAN)

25/03/2013 - INDEV
    +NOTE 'THE GAME WILL BE DELAYED,ALSO IT WILL BE RELEASED WITH HUMAN RACE ONLY'
    +NOTE 'OTHER RACES WILL BE ADDED IN ALPHA AND BETA'
    +NOTE 'I MUST PUT RADU AND VLAD TO WORK'
    +DEBUGGING SESSION STARTED

27/03/2013 - INDEV
    +CHANGED ONE LOADING SCREEN TITLE
    +CRACK TEAM HAS A LOGO
    +CRACK TEAM HAS A WEBSIT
    +CRACK TEAM WESITE IS READY
    +ADDED (qqq) TYPE CORD. FOR EACHLINE
    +PLAINS LOCATION ADDED TO HUMAN KINGDOM
    +MAP X,Y HELPER
    +REACHED 7000 LINES OF CODE
    +PROGRAM HAS 1.12 MB
    +FINISHED 1/8 OF THE MAP MOVEMENT
    +MOTIVATIONAL NOTE 'I MUST DO AT LEAST 5/8 MORE TOMMOROW'

28/03/2013  - INDEV
    +2/8 OF MAP MOVEMENT COMPLETE
    +3/8 OF MAP MOVEMENT COMPLETE
    +4/8 OF MAP MOVEMENT COMPLETE
    +5/8 OF MAP MOVEMENT COMPLETE
    +SECRET MAP MARKER
    +ADDED LUCKY 777
    +DEMON ALTAR ADDED ON THE MAP
    +TEMPLE      ADDED ON THE MAP

17/04/2013 - INDEV
    +6/8 OF MAP MOVEMENT COMPLETE
    +7/8 OF MAP MOVEMENT COMPLETE
    +8/8 OF MAP MOVEMENT COMPLETE
    +NOTE "OMG I FINISHED THAT F MAP!, STARTING THE REAL GAME"

18/04/2013 - INDEV
    +ADDED LOCATIONHISTORY TO KEEP TRACK OF THE LOCATION IN OTHER SEGMENTS OF THE GAME

19/04/2013 - INDEV
    +ADDED goto lh; , goto lc;

27/04/2013 - INFDEV.RS
    +SWITCHED TO INFDEV.RS (INFINITE DEVELOPMENT RESERCH STOP)
    +RESERCH (NEW MAP MOVMENT, IT SHOULD TAKE LESS SPACE, ALSO WILL MAKE THE MAP BIGGER AND MORE REALISTIC)
    +RESERCH (FAKE MULTITHREADING, MAKES INPUT AND OUTPUT AT THE SAME TIME (FAKE INPUT))
    +NOTE    (MIGHT STOP WORKING ON "GAME OF STATS" AND START ANOTHER PROJECT , BETTER AND NICER)

28/04/2013 - INFDEV.RS
    +RESERCH (MENU APPERENCE)
    +RESERCH (TEXT ART FILLING)



*/
/* TAGS

ADMIN
+999 EVRYTHING

HOLY
+10% DAMAGE TO EVIL

EVIL
+10% DAMAGE TO GOOD

BRUTAL
+5% DAMAGE
-1 ENDU

WISE
+5% XP
+1 INTE
-1 STRE

LUCKY
+5 LUCK
-1 INTE
-1 ENDU

*/
/* SECURITY
//----------------------------------------------------------------------------------------START SECURITY---------------------------------
string userName;
int getch();
int pass1,pass2,pass3,pass4;
int tr=3;

login:
    system ("color F0");
system ("cls");
system ("ECHO %TIME%");
cout << endl << endl << tr << endl;
cout << endl << "USERNAME:";
cin  >> userName;
cout << endl << "PASSWORD:";
pass1= getch();
pass2= getch();
pass3= getch();
pass4= getch();

if ((userName=="root")&&(pass1==2)&&(pass2==0)&&(pass3==0)&&(pass4==8))
    goto loginsuc;
else
{
tr=tr-1;
if (tr==0)
{
    system ("cls");
    system ("color AC");
    cout << "LOCKDOWN ACTIVATED..." << endl << "WAIT 300s TO RELOG" << endl;
       Sleep(300000);
       tr=1;
       goto login;
}
}
goto login;
//-----------------------------------------------------------------------------------------END SECURITY----------------------------------
loginsuc:
*/
/* BUG LOG

18/03/2013 - INDEV
    +BUG 0001a DISCOVERED
        ( IN ANY CLASS MENU , YOU CAN SKIP THE SELECTION )
    +BUG 0001a FIXED

    +BUG 0002a DISCOVERED
        (THE ORC CLASS SELECT IS BROKEN)
    +BUG 0002a FIXED

    +BUG 0003a DISCOVERED
        (HP WAS DIPLAYED AS 10.0 INSTEAD OF 100.0)
    +BUG 0003a FIXED

    +BUG 0004a DISCOVERED
        (THERE IS A DELAY AFTER YOU ENTER THE NAME)
    +BUG 0004a FIXED

19/03/2013
    +BUG 0003b DISCOVERED
        (THE HP IS DISPLAYED AS 0 INSTEAD OF 100.0)
        (THE HP VALUE WAS WRITEN AS MP AT THE END)
    +BUG 0003b FIXED

21/03/2013
    +BUG 0005a DISCOVERED
        (THE ONLY COLOR COMBINATION IS RED TEXT WITH ANY BACKGROUND)
        (WHITE BACKGROUND WON'T WORK WITH RED TEXT)
        (BACKGROUNDS WON'T WORK WITHOUT RED TEXT)
    +BUG 0005a FIXED

25/03/2013
    +BUG 0006a DISCOVERED
        (THE ch==getch(); IS NOT GETTING AN INPUT)
        (YOU CAN'T MOVE ON THE MAP)
        (THE PROBLEM COULD BE AT THE if ALSO)
        (CHANGED ch7 FROM char TO int, NO CHANGE)
        (DERP HERP, ch7==getch(); REPLACED WITH ch7=getch();)
    +BUG 006a DIXED

    +BUG 0006b DISCOVERED
        (INSTEAD OF GOING LEFT WHEN a IS PRESSED, IT GOES RIGHT)
        (INSTEAD OF GOING RIGHT WHEN d IS PRESSED, IT GOES LEFT)
        (DERP HERP d=100 a=97)
    +BUG 0006b FIXED)

28/03/2013
    +BUG 0007a DISCOVERED
        (LUCKY 777 DOESN'T WORK)
        (IT LOOPS BACK TO GAMBLE:)
        (CHANGED FROM INT TO ASCII)
    +BUG 0007a FIXED

*/
/* COLORS

color 0(back) 1(text)

c1= red      =C
c2= blue     =9
c3= green    =A
c4= yellow   =E
c5= black    =0

b1= red      =4
b2= blue     =1
b3= green    =2
b4= yellow   =6
b5= white    =F

if ((c1==1)&&(b1==1))
    system("color 4C");
if ((c1==1)&&(b2==1))
    system("color 1C");
if ((c1==1)&&(b3==1))
    system("color 2C");
if ((c1==1)&&(b4==1))
    system("color 6C");
if ((c1==1)&&(b5==1))
    system("color FC");

if ((c1==2)&&(b1==1))
    system("color 49");
if ((c1==2)&&(b2==1))
    system("color 19");
if ((c1==2)&&(b3==1))
    system("color 29");
if ((c1==2)&&(b4==1))
    system("color 69");
if ((c1==2)&&(b5==1))
    system("color F9");

if ((c1==3)&&(b1==1))
    system("color 4A");
if ((c1==3)&&(b2==1))
    system("color 1A");
if ((c1==3)&&(b3==1))
    system("color 2A");
if ((c1==3)&&(b4==1))
    system("color 6A");
if ((c1==3)&&(b5==1))
    system("color FA");

if ((c1==4)&&(b1==1))
    system("color 4E");
if ((c1==4)&&(b2==1))
    system("color 1E");
if ((c1==4)&&(b3==1))
    system("color 2E");
if ((c1==4)&&(b4==1))
    system("color 6E");
if ((c1==4)&&(b5==1))
    system("color FE");

if ((c1==5)&&(b1==1))
    system("color 40");
if ((c1==5)&&(b2==1))
    system("color 10");
if ((c1==5)&&(b3==1))
    system("color 20");
if ((c1==5)&&(b4==1))
    system("color 60");
if ((c1==5)&&(b5==1))
    system("color F0");

*/
/* MAP TEMPLATE
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#1
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //@2
cout <<"  #                                                                      #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#3
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //@4
cout <<"  #                                                                      #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //@6
cout <<"  #                                                                      #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //@8
cout <<"  #                                                                      #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

*/
/* MAP X,Y HELPER

 qq wq eq rq tq yq uq iq

 qw ww ew rw tw yw uw iw

 qe we ee re te ye ue ie

 qr wr er rr tr yr ur ir

 qt wt et rt tt yt ut it

 qy wy ey ry ty yy uy iy

 qu wu eu ru tu yu uu iu

 qi wi ei ri ti yi ui ii

*/
/* TURN PLANS

    1.PLAYER MOVE
    2.SAVE DIRECTION
    3.GOTO RAN EVENT
/\  4.GET EVENT
|   5.GOTO EVENT
|   6.END EVENT
|   7.RETURN T LOCATION
 \______LOOP



*/
#include <iostream>         //basic
#include <string>           //for text values
#include <conio.h>          //for getch();
#include <stdio.h>          //basic
#include <windows.h>        //for DOS
#include <cstdlib>          //for random
#include <ctime>            //for time settings

using namespace std;

int main()
{
debug:  //RESETS THE GAME VARIABLES AND SETTINGS

system ("title GAME OF STATS / BOOT / INFDEV.RS");                             //GAME TITLE
system ("color F0");                                                       //MAIN COLOR

int getch();                                                               //ONE INPUT
// 49 is 1


    char a,b,c,d,e;     // DESIGN CHAR ASCII
    a=219;              // |
    b=223;              // --
    c=220;              // __
    d=987;              // |||
    e=254;              // ||

    float fdmg,efdmg,xp,fdefe,attspe,ehp,hp;                        //floats

    int maxhp,mp,dmgmax,dmgmin,lvl,def;                             //stats 1
    int inte,stre,endu,vita,dext,luck;                              //stats 2
    int emp,edmgmax,edmgmin,edef,elvl;                              //enemy stats 1
    int einte,estre,eendu,evita,edext,eluck;                        //enemy stats 2
    int gold;                                                       //items

    int day;                                                        //events

    int c1,c2,c3,c4,c5;                                             //text clors
    c1=c2=c3=c4=c5=0;
    c5=1;
    int b1,b2,b3,b4,b5;                                             //background colors
    b1=b2=b3=b4=0;
    b5=1;

    char ch,ch1,ch2,ch3,ch4,ch5,ch6;                                //choice
    int ch7;
    //ch7 is the map direction

    int l0,l1;                                                      //lucky 777

    int  ran1,ran2;                                                 //random

    string username,race,tag,sex,title,cla,name,age;                //boosts

    string locationhistory;

//human     mage,archer,ninja,paladin,knight,warrior
//elf       archer,mage,assassin,knight,joker,thief
//dwarf     warlord,engineer,warrior,knight
//orc       shaman,warrior,warlord
//giant     cyclops,colosal

{ //RANDOM GENERATOR            COMPLETE

    srand((unsigned)time(0));
        ran1 = (rand()%10)+1;        //LOADING SCREEN
        ran2 = (rand()%100)+1;       //LUCKY 777


}
{ //LOADING SCREEN              COMPLETE
cout << endl;
load:
system ("title GAME OF STATS / LOADING / INFDEV.RS");

system ("cls");

cout << endl<<"                                     "<<"LOADING..."<<endl;
cout << endl<<"                            "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<< endl;
cout << endl;
if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (500);
system ("cls");

cout << endl<<"                                     "<<"LOADING   "<<endl;
cout << endl<<"                            "<<e<<d<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<< endl;
cout << endl;
if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (600);
system ("cls");

cout << endl<<"                                     "<<"LOADING.  "<<endl;
cout << endl<<"                            "<<e<<d<<e<<d<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<< endl;
cout << endl;
if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (600);
system ("cls");

cout << endl<<"                                     "<<"LOADING.. "<<endl;
cout << endl<<"                            "<<e<<d<<e<<d<<e<<d<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<< endl;
cout << endl;
if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (600);
system ("cls");

cout << endl<<"                                     "<<"LOADING..."<<endl;
cout << endl<<"                            "<<e<<d<<e<<d<<e<<d<<e<<d<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<< endl;
cout << endl;
if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (600);
system ("cls");

cout << endl<<"                                     "<<"LOADING   "<<endl;
cout << endl<<"                            "<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<< endl;
cout << endl;
if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (600);
system ("cls");

cout << endl<<"                                     "<<"LOADING.  "<<endl;
cout << endl<<"                            "<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<< endl;
cout << endl;
if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (600);
system ("cls");

cout << endl<<"                                     "<<"LOADING.. "<<endl;
cout << endl<<"                            "<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<< endl;
cout << endl;
if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (600);
system ("cls");

cout << endl<<"                                     "<<"LOADING..."<<endl;
cout << endl<<"                            "<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<" "<<e<<" "<<e<<" "<<e<<" "<<e<< endl;
cout << endl;
if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (600);
system ("cls");

cout << endl<<"                                     "<<"LOADING   "<<endl;
cout << endl<<"                            "<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<" "<<e<<" "<<e<<" "<<e<< endl;
cout << endl;
if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (600);
system ("cls");

cout << endl<<"                                     "<<"LOADING.  "<<endl;
cout << endl<<"                            "<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<" "<<e<<" "<<e<< endl;
cout << endl;
if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (600);
system ("cls");

cout << endl<<"                                     "<<"LOADING.. "<<endl;
cout << endl<<"                            "<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<" "<<e<< endl;
cout << endl;
if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (600);
system ("cls");

cout << endl<<"                                     "<<"LOADING..."<<endl;
cout << endl<<"                            "<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<<d<<e<< endl << endl;

if (ran1==1) cout << "You are cool.";if (ran1==2) cout << "Do you belive?";if (ran1==3) cout << "No Easter eggs in this game!";if (ran1==4) cout << "This game doesn't have bugs, it has RANDOM features.";if (ran1==5) cout << "We are beeing watched.";
if (ran1==6) cout << "2+2=YOU";if (ran1==7) cout << "BEHINDE YOU!!!";if (ran1==8) cout << "Game plays you!";if (ran1==9) cout << "Lama,Lama,Lama,Lama,Lama,Lama,Duck";if (ran1==10) cout << "Yo MAMA!!!";

Sleep (600);
system ("cls");
}
{ //MAIN MENU                   COMPLETE
menu:
system ("title GAME OF STATS / MAIN MENU / INFDEV.RS");
    system ("cls");

    cout <<  endl;

    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"          GAME OF STATS         "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"       -A  NO BUDGET GAME-      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"       -A TEXT BASED GAME-      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"1.NEW GAME                      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"2.OPTIONS                       "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"3.CREDITS                       "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"4.DEBUGER                       "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"5.EXIT                          "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

    ch1 = getch();

    system("cls");

if (ch1==49)
    goto newgame;
else
if (ch1==50)
    goto options;
else
if (ch1==51)
    goto credits;
else
if (ch1==52)
    goto debug;
else
if (ch1==53)
    goto exit;

if (ch1==55)
    {
    ch1 = getch();
    if (ch1==55)
            {
            ch1 = getch();
            if (ch1==55)
            goto lotto;
            }
    }
        else
        goto menu;
}
{ //NEW GAME                    COMPLETE
newgame:
system ("title GAME OF STATS / TUTORIAL / INFDEV.RS");
cout << endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"            TUTORIAL            "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" GAME OF STATS, IS A TEXT BASED "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" GAME WITH RPG ELEMENTS , THIS  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" MEANS THERE IS NO 3D WORLD AND "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" MOST ACTIONS ARE TYPED IN THE  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" CONSOLE                        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" THERE ARE DIFFERENT TYPES OF   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" INPUTS FOR THE CONSOLE:        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" #_        SIMPLE ACTION        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" $_        SHOP/INVENTORY ACTION"                                                            <<a<<  endl;
    cout <<"                         "<<a<<" @_        SELF INFORMATION     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" THERE ARE DIFFERENT TYPES OF   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" OUTPUTS AS WELL:               "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  #_       SIMPLE EVENT         "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  $_       SHOP EVENT           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  @_       STORY EVENT          "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  <NAME>_  CHAT EVENT           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  T_       TIPS/TUTORIAL        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  !_       WARNING              "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;
getch();
system ("cls");
system ("title GAME OF STATS / STORY / INFDEV.RS");
cout << endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"             STORY              "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" Welcome to a world where every-"                                                            <<a<<  endl;
    cout <<"                         "<<a<<" -thing was nice and peacefull, "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" but iyt if nowhere the force of"                                                            <<a<<  endl;
    cout <<"                         "<<a<<" evil came blazing down killing "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" and destroying everything in   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" their way.                     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" And all of this happened becau-"                                                            <<a<<  endl;
    cout <<"                         "<<a<<" -se of a single monster...     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" people call him the Devil, but "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" it is way worse than that.     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" He is... The Lord of The Void  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" Old pices of knoledge speak    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" about a place where nothing    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" and everything can be found,   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" a place without end or begging,"                                                            <<a<<  endl;
    cout <<"                         "<<a<<" a place where time and space   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" can be bent.During all this    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" time it held such an evil army "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" .The Void oppened using the    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" Rapture ,a misterious gate.    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" When Arret (Earth) was invaded "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" ,all kingdoms came united as   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" one to fight the armys of evil."                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" Now it is your time to rise and"                                                            <<a<<  endl;
    cout <<"                         "<<a<<" shine, to chose your destiny,  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" who you are and what you will  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" become.                        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;
getch();
}
{ //RACE MENU                   COMPLETE
race:
    system ("title GAME OF STATS / RACE SELECT / INFDEV.RS");
system ("cls");
cout << endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"             RACE               "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<" HUMAN- ARE THE BASIC RACE      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"        THEY ARE GOOD AT FINDING"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"        GOLD AND GETTING XP.    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" ELF-   HAVE FAST ATTACK SPEED  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"        THEY ARE GOOD AT MAGIC  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"        ATTACKS AND DODGING.    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" DWARF- ARE STRONG AND RESISTANT"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"        THEY ARE GOOD AT THE    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"        ART OF CRAFTING.        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" ORC-   ARE STUPID BUT VERY     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"        STRONG WHEN IN COMBAT   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"        THEY CAN BOOST THEIR    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"        POWER,RAGGING           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" GIANT- ARE AS STUPID AS TALL.  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"        GIANTS ARE SLOW BUT THEY"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"        CAN ENDURE LOTS OF HITS."                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" PLEASE USE lowercase TO CHOSE  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" A RACE BY JUST TYPING ITS NAME "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

cin >>race;

if (race=="human")
    goto human;

if (race=="elf")
    goto elf;

if (race=="dwarf")
    goto dwarf;

if (race=="orc")
    goto orc;

if (race=="giant")
    goto giant;

goto race;
}
{ //RACE STATS                  COMPLETE
human:
    system ("cls");
cout <<"HUMAN";

hp=100;
mp=10;
stre=5;
endu=5;
vita=6;
inte=6;
dext=6;
luck=5;
dmgmax=5;
dmgmin=1;
attspe=10;

goto sex;
getch();

elf:
    system ("cls");
cout <<"ELF";

hp=100;
mp=20;
stre=4;
endu=4;
vita=5;
inte=6;
dext=7;
luck=5;
dmgmax=5;
dmgmin=1;
attspe=12;

goto sex;
getch();

dwarf:
    system ("cls");
cout <<"DWARF";

hp=100;
mp=0;
stre=6;
endu=6;
vita=5;
inte=7;
dext=2;
luck=5;
dmgmax=5;
dmgmin=1;
attspe=9;

goto sex;
getch();

orc:
    system ("cls");
cout <<"ORC";

hp=120;
mp=0;
stre=8;
endu=7;
vita=5;
inte=2;
dext=4;
luck=5;
dmgmax=5;
dmgmin=2;
attspe=8;

goto sex;
getch();

giant:
    system ("cls");
cout <<"GIANT";

hp=120;
mp=0;
stre=5;
endu=6;
vita=8;
inte=2;
dext=5;
luck=4;
dmgmax=6;
dmgmin=2;
attspe=6;

goto sex;
getch();
}
{ //GENDER MENU                 COMPLETE
sex:
    system ("title GAME OF STATS / GENDER SELECT / INFDEV.RS");
system ("cls");
cout << endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"            GENDER              "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<" MALE-    STRONG AND RESISTANT  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" FEMALE-  HIGH VITALITY AND MANA"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" PLEASE USE lowercase TO CHOSE  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" A GENDER BY JUST TYPING IT     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

cin >> sex;

if (sex=="male")
    {
endu=endu+1;
stre=stre+1;
goto cla;
    }
else
if (sex=="female")
    {
vita=vita+1;
mp=mp+10;
goto cla;
    }
else
goto sex;
}
{ //CALSS MENU                  COMPLETE
cla:
   system ("cls");

if (race=="human")
    goto chuman;

if (race=="elf")
    goto celf;

if (race=="dwarf")
    goto cdwarf;

if (race=="orc")
    goto corc;

if (race=="giant")
    goto cgiant;


chuman:
system("cls");
system ("title GAME OF STATS / HUMAN CLASS SELECT / INFDEV.RS");
cout << endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"          HUMAN CLASS           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<" MAGE-    ALL MAGES ARE         "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          INTELIGENT FULL OF    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          MANA POWER            "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" NINJA-   ALL NINJAS ARE        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          VERY FAST,THEY HAVE   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          LOTS OF DEXTERITY     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" PALADIN- THEY ARE STRONG AND   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          AND RESISTANT AND     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          THEY HAVE A STRONG    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          BELIFE FOR GOD        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" KNIGHT- THEY SERVED IN ALL WARS"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         THEY ARE STRONG AND    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         BRAVE                  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" MERC-   THEY SEE THE WAR AS A  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         PROFITABLE EVENT       "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         MERCS HAVE DEXT        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         AND VITA               "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" PLEASE USE lowercase TO CHOSE  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" A CLASS BY JUST TYPING IT      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

cin >> cla;

if ((cla=="mage")&&(race=="human"))
goto humanmage;
if ((cla=="ninja")&&(race=="human"))
goto humanninja;
if ((cla=="paladin")&&(race=="human"))
goto humanpaladin;
if ((cla=="knight")&&(race=="human"))
goto humanknight;
if ((cla=="merc")&&(race=="human"))
goto humanmerc;

goto chuman;

humanmage:
mp=mp+50;
inte=inte+2;
goto shuman;

humanninja:
dext=dext+2;
attspe=attspe+3;
goto shuman;

humanpaladin:
hp=hp+20;
mp=mp+20;
stre=stre+1;
endu=endu+1;
goto shuman;

humanknight:
stre=stre+2;
dmgmax=dmgmax+2;
goto shuman;

humanmerc:
vita=vita+1;
dext=dext+1;
luck=luck+2;
goto shuman;

celf:
system("cls");
system ("title GAME OF STATS / ELF CLASS SELECT / INFDEV.RS");
cout << endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"            ELF CLASS           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<" ARCHER-  GOT SHARP EYES AND    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          LOTS OF DEXTERITY AND "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          EXTRA INTEIGENCE      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" MAGE-    ELF MAGES ARE KNOWN   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          FOR THE ANCIENT MAGIC "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          SKILLS                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" ASSASSIN-THEY ARE THE NINJAS OF"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          THE ELFS,THEY HAVE    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          LOTS OF DEXTERITY AND "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          ATTACK SPEED          "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" KNIGHT- THEY SERVED IN ALL WARS"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         THEY ARE STRONG AND    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         BRAVE                  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" THIEF-  THEY SEE THE WAR AS A  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         PROFITABLE EVENT       "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         THEY HAVE DEXT         "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         VITA,INTE AND ATTACK   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" PLEASE USE lowercase TO CHOSE  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" A CLASS BY JUST TYPING IT      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;


cin >> cla;

if ((cla=="archer")&&(race=="elf"))
    goto elfarcher;
if ((cla=="mage")&&(race=="elf"))
    goto elfmage;
if ((cla=="assassin")&&(race=="elf"))
    goto elfassassin;
if ((cla=="knight")&&(race=="elf"))
    goto elfknight;
if ((cla=="thief")&&(race=="elf"))
    goto elfthief;

goto celf;

elfarcher:
dext=dext+2;
inte=inte+1;
goto self;

elfmage:
mp=mp+50;
inte=inte+1;
goto self;

elfassassin:
dext=dext+2;
attspe=attspe+3;
goto self;

elfknight:
stre=stre+1;
endu=endu+1;
hp=hp+10;
goto self;

elfthief:
dext=dext+2;
inte=inte+1;
vita=vita+1;
attspe=attspe+1;
goto self;

cdwarf:
system("cls");
system ("title GAME OF STATS / DWARF CLASS SELECT / INFDEV.RS");
cout << endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"          DWARF CLASS           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<" WARLORD- STRONG AND FULL OF    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          HEALTH,THEY ARE THE   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          GUARDIANS OF DWARFS   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" ENGINEER-ARE VERY INTELIGENT   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          AND SKILLED IN THE ART"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          OF CRAFTING           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" WARRIOR- THEY ARE THE FRESH    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          RECRUITS OF THE       "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          DWARF KINGDOM,THEY    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          HAVE A LOT TO LEARN   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" KNIGHT- THEY SERVED IN ALL WARS"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         THEY ARE STRONG AND    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         BRAVE                  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" PLEASE USE lowercase TO CHOSE  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" A CLASS BY JUST TYPING IT      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;


cin >> cla;

if ((cla=="warlord")&&(race=="dwarf"))
    goto dwarfwarlord;
if ((cla=="engineer")&&(race=="dwarf"))
    goto dwarfengineer;
if ((cla=="warrior")&&(race=="dwarf"))
    goto dwarfwarrior;
if ((cla=="knight")&&(race=="dwarf"))
    goto dwarfknight;

goto cdwarf;

dwarfwarlord:
stre=stre+2;
vita=vita+2;
goto sdwarf;

dwarfengineer:
inte=inte+3;
dext=dext+2;
endu=endu-1;
goto sdwarf;

dwarfwarrior:
stre=stre+1;
vita=vita+1;
endu=endu+1;
goto sdwarf;

dwarfknight:
stre=stre+2;
endu=endu+2;




corc:
system("cls");
system ("title GAME OF STATS / ORC CLASS SELECT / INFDEV.RS");
cout << endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"            ORC CLASS           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<" SHAMAN-  STRONG AND FULL OF    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          MAGIC,THEY ARE ALSO   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          GREAT HEALLERS        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" WARRIOR- STRONG AND RESISTANT  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          ALSO A BIT DUMB       "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          GOOD IN COMBAT        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" WARLORD- STUPID AND SLOW       "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          BUT THEY ARE THE      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          STRONGEST AND MOST    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          RESISTANT ON ARRET    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" KNIGHT- THEY SERVED IN ALL WARS"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         THEY ARE STRONG AND    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"         BRAVE                  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" PLEASE USE lowercase TO CHOSE  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" A CLASS BY JUST TYPING IT      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

cin >> cla;


if ((cla=="shaman")&&(race=="orc"))
    goto orcshaman;
if ((cla=="warrior")&&(race=="orc"))
    goto orcwarrior;
if ((cla=="warlord")&&(race=="orc"))
    goto orcwarlord;

goto corc;

orcshaman:
mp=mp+50;
stre=stre+1;
goto sorc;

orcwarrior:
stre=stre+2;
endu=endu+1;
inte=inte-1l;
goto sorc;

orcwarlord:
stre=stre+3;
endu=endu+3;
vita=vita+1;
attspe=attspe-1;
inte=inte-2;
goto sorc;

cgiant:
system ("cls");
system ("title GAME OF STATS / GIANT CLASS SELECT / INFDEV.RS");
cout << endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"          GIANT CLASS           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<" CYCLOPS- STRONG AND FULL OF    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          LIFE , BUT SLOW AND   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          STUPID                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" COLOSAL- RESISTANT AND STRONG  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          THEY ARE GREAT TANKS  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"          AND GOOD FOR ATTACK   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" PLEASE USE lowercase TO CHOSE  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" A CLASS BY JUST TYPING IT      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

cin >> cla;

if ((cla=="cyclops")&&(race=="giant"))
    goto giantcyclops;
if ((cla=="colosal")&&(race=="giant"))
    goto giantcolosal;

goto cgiant;

giantcyclops:
hp=hp+50;
stre=stre+2;
inte=inte-3;
goto sgiant;

giantcolosal:
endu=endu+4;
stre=stre+1;
inte=inte-3;
goto sgiant;
}
{ //STORY & NAME                COMPLETE
shuman:
    system ("title GAME OF STATS / NAME SELECT / INFDEV.RS");
    system("cls");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"              NAME              "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" NOW TELL US, WHAT IS THE NAME  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" YOU WANT US TO CALL YOU        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

    cin >> username;
if (username=="Napoleon")           //RACISM AT ITS BEST
{
cout << "Ahh... Croasants";
Sleep (2000);
}
system("cls");
system ("title GAME OF STATS / HUMAN STORY / INFDEV.RS");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"          HUMAN STORY           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU ARE ONE OF THE MANY HUMANS"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  THAT RULE THIS LAND.          "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU WERE BRON IN A VILLAGE    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  NAMED AIAOSOGOM BUT AT THE    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  OF 18 YOU JOINED THE WAR      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  AGAINST THE ELF RACE ,        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  NOW YOU ARE 21, AND YOU WERE  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  CALLED TO JOIN THE ALLIANCE   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  OF HUMANS,ELFS,DWAWRFS AND    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  ORCS AGAINST THE EVIL ARMY OF "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  THE VOID.                     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  AT THE MOMENT YOU LIVE IN THE "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  CITADEL, ONE OF THE REMAINING "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  STRONGHOLDS OF HUMANS         "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOUR MAIN QUEST IS TO STOP THE"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  ARMYS OF THE VOID.            "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOUR SECONDARY QUEST IS TO    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  FIND YOUR FAMILY.             "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

getch();
goto review;

self:
    system("cls");
    system ("title GAME OF STATS / NAME SELECT / INFDEV.RS");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"              NAME              "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" NOW TELL US, WHAT IS THE NAME  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" YOU WANT US TO CALL YOU        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

    cin >> username;
if (username=="Napoleon")           //RACISM AT ITS BEST
{
cout << "Ahh... Croasants";
Sleep (2000);
}
system("cls");
system ("title GAME OF STATS / ELF STORY / INFDEV.RS");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"           ELF STORY            "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" YOU ARE AN ELF,ONE OF THE      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" RACES KNOW ON ARRET            "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU WERE BORN IN ISAI,THE     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  CAPITAL CITY OF TAUOWS        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU LIVED YOUR LIFE AS A GUARD"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  SINCE YOU WERE 20 YEARS OLD   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  NOW YOU ARE 23,AND THE COUNCIL"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  OF TAUOWS ORDERD YOU TO FIGHT "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  IN THE ALLIANCE FORGED BY THE "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  DWARFS AGAINS THE ARMY OF THE "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  VOID.                         "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  THE DAY AFTER THE ORDER YOU   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  TAKE A CARAVAN TO THE STRONG  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  DWARF FORT NOLOG.             "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOUR MAIN QUEST IS TO SECURE  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  THE LAND AROUND NOLOG         "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOUR SECONDARY QUEST IS TO    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  DECRYPT THE ELDES RIDDLE.     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

getch();
goto review;

    sdwarf:
        system("cls");
        system ("title GAME OF STATS / NAME SELECT / INFDEV.RS");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"              NAME              "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" NOW TELL US, WHAT IS THE NAME  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" YOU WANT US TO CALL YOU        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

    cin >> username;
if (username=="Napoleon")           //RACISM AT ITS BEST
{
cout << "Ahh... Croasants";
Sleep (2000);
}
system("cls");
system ("title GAME OF STATS / DWARF STORY / INFDEV.RS");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"         DWARF STORY            "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" YOU ARE AN DWARF,THE MOST      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" DEVELOPED KINGDOM ON ARRET     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU WERE BORN ON THE FRONT    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  DURING THE WAR WITH THE ELFS  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU LIVED YOUR LIFE AS A SMITH"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  BUT YOU ALWASY WANTED TO BE A "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  WARRIOR,WHEN THE ARMYS OF THE "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  VOID ATTACKED THE DWARFES     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  STARTED AN ALLIANCE.YOU SIGNED"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  UP TO FIGHT IN THE FIRST LINE "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  HOPPING TO CHNAGE YOUR LIFE.  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU JOINED A WAGON TO NOGOL   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  ,ONE THE REMAINING FORTS      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  ALSO A STRATEGIC POINT        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOUR MAIN QUEST IS TO RETAKE  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  THE DWARVEN LAND              "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOUR SECONDARY QUEST IS TO    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  SEND THE ARMYS OF VOID BACK   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

getch();
goto review;

sorc:
    system("cls");
system ("title GAME OF STATS / NAME SELECT / INFDEV.RS");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"              NAME              "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" NOW TELL US, WHAT IS THE NAME  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" YOU WANT US TO CALL YOU        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

    cin >> username;
if (username=="Napoleon")           //RACISM AT ITS BEST
{
cout << "Ahh... Croasants";
Sleep (2000);
}
system("cls");
system ("title GAME OF STATS / ORC STORY / INFDEV.RS");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"            ORC STORY           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU ARE ONE OF THE LAST ORCS  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  ,MOST OF YOU HAVE BEEN KILLED "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU DON'T KNOW WHERE YOU WERE "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  BORN,ALL YOU WANT IS A NORMAL "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  LIFE,BUT AT THE AGE OF 25     "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU WERE ENSLAVED BY THE      "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  HUMANS.ONE YEAR LATER THE WAR "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  STARTED AND THE ALLIANCE ASKED"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  FOR THE HELP OF THE ORCS,YOU  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  WERE FREE NOW,BUT YOU DECIDED "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  TO JOIN THE WAR               "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU ARE NOW IN THE OUTSKIRTS  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  OF THE CITADEL WAITING FOR    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  THE MARCH TO NOGOL            "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOUR MAIN QUEST IS TO STOP THE"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  ARMYS OF THE VOID.            "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOUR SECONDARY QUEST IS TO    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  JOIN THE ORC ARMY             "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

getch();
goto review;

sgiant:
    system("cls");
    system ("title GAME OF STATS / NAME SELECT / INFDEV.RS");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"              NAME              "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" NOW TELL US, WHAT IS THE NAME  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<" YOU WANT US TO CALL YOU        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

    cin >> username;
if (username=="Napoleon")           //RACISM AT ITS BEST
{
cout << "Ahh... Croasants";
Sleep (2000);
}
system("cls");
system ("title GAME OF STATS / GIANT STORY / INFDEV.RS");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"          GIANT STORY           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU ARE THE LAST GIANT        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  ALL OTHER HAVE BEEN KILLED    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU WERE BRON IN A CAVE       "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  DURING THE GIANT HUNT YOUR    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  PARENTS DIED, YOU WERE HIDDEN "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  DEEPER IN THE CAVES.          "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU LIVED YOUR LIFE IN THE    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  SAME CAVE FOR 40 YEARS BUT    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  SINCE THE WAR STARTED YOU WANT"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  T FIGHT HAND TO HAND WITH THE "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  OTHER RACES.                  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOU SART TO MAKE YOUR WAY TO  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  THE CITADE,THE HUMAN CAPETOWN "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  ,YOU HOPE TO JOIN THEIR ARMY  "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOUR MAIN QUEST IS TO STOP THE"                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  ARMYS OF THE VOID.            "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  YOUR SECONDARY QUEST IS TO    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"  JOIN THE ALLIANCE.            "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"                                "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

getch();
goto review;
}
{ //REVIEW MENU                 COMPLETE
review:
    system ("title GAME OF STATS / CHAR REVIEW / INFDEV.RS");
system("cls");
cout << endl;

cout <<"                         "<<"USERNAME-----"<<username   <<endl;
cout <<"                         "<<"GENDER-------"<<sex        <<endl;
cout <<"                         "<<"RACE---------"<<race       <<endl;
cout <<"                         "<<"CLASS--------"<<cla        <<endl;
cout <<"                         "<<"HP-----------"<<hp         <<endl;
cout <<"                         "<<"MP-----------"<<mp         <<endl;
cout <<"                         "<<"INTE---------"<<inte       <<endl;
cout <<"                         "<<"VITA---------"<<vita       <<endl;
cout <<"                         "<<"DEXT---------"<<dext       <<endl;
cout <<"                         "<<"STRE---------"<<stre       <<endl;
cout <<"                         "<<"LUCK---------"<<luck       <<endl;
cout <<"                         "<<"ENDU---------"<<endu       <<endl;
cout <<"                         "<<"ATTACKSPEED--"<<attspe     <<endl;
cout <<"                         "<<"DMG MAX------"<<dmgmax     <<endl;
cout <<"                         "<<"DMG MIN------"<<dmgmin     <<endl;

cout << endl;

cout <<"                         "<<"ARE THESE STATS OK WITH YOU?"<<endl;
cout <<"                         "<<"1.YES"<<endl;
cout <<"                         "<<"2.NO "<<endl;

ch6 = getch();

if (ch6==49)
goto gps;
if (ch6==50)
goto menu;
else goto review;
}
{ //MAP TEMPLATES               WIP
gps:
    system("cls");
cout << endl;
system ("title GAME OF STATS / FULL MAP / INFDEV.RS");


cout <<"  ###########################################################################"<<endl;
cout <<"  #                              #               ISAI             #         #"<<endl;
cout <<"  #                             #               X                #          #"<<endl;
cout <<"  #                            #     ELF                        #           #"<<endl;
cout <<"  #      HUMAN                 #      KINGDOM                  #            #"<<endl;
cout <<"  #        KINGDOM             #                              #             #"<<endl;
cout <<"  #                           #                              #              #"<<endl;
cout <<"  #                           #                  STEAR       #              #"<<endl;
cout <<"  #                           #       ####      X           #               #"<<endl;
cout <<"  #     CITADEL                #     #    #                #                #"<<endl;
cout <<"  #    X                        #####      ################                 #"<<endl;
cout <<"  #             ISTER          #                          #     VOID        #"<<endl;
cout <<"  #            X              #                           #   EMPIRE        #"<<endl;
cout <<"  #                          #             THE            #                 #"<<endl;
cout <<"  #                   #######              BATTLEFIELD    #      PORTAL     #"<<endl;
cout <<"  #                  #                          X         #     X           #"<<endl;
cout <<"  #                 #                                     #                 #"<<endl;
cout <<"  #          #######        DWARF KINGDOM                 #                 #"<<endl;
cout <<"  #         #                                              #                #"<<endl;
cout <<"  #         #                                     HILLS     #               #"<<endl;
cout <<"  #        #                                     X           #              #"<<endl;
cout <<"  #########                                                   #             #"<<endl;
cout <<"  #                                   NOGOL                  #              #"<<endl;
cout <<"  #                                  X                      #               #"<<endl;
cout <<"  #          SECRET FOREST                                 #                #"<<endl;
cout <<"  #         X                                             #                 #"<<endl;
cout <<"  #                                                      #                  #"<<endl;
cout <<"  ###########################################################################"<<endl;
getch();
cout <<endl;

if (race=="human")
goto citadel;
if (race=="elf")
goto citadel;
if (race=="dwarf")
goto nogol;
if (race=="orc")
goto forest;
if (race=="giant")
goto hill;

citadel:
{ //HUMAN KINGDOM
{ //HUMAN KINGDOM MAIN MAP
        system("cls");
    system ("title GAME OF STATS / HUMAN MAP / INFDEV.RS");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                          OAK FOREST              #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                 THE CITADEL                                          #"<<endl; //#
cout <<"  #                                                           ELF        #"<<endl; //#5
cout <<"  #                                                          BORDER      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                          ISTER FORT                  #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX DWARF                        #"<<endl; //@
cout <<"  #   X                               XXXXX BORDER                       #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                          OAK FOREST              #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                 THE CITADEL                                          #"<<endl; //#
cout <<"  #                                                           ELF        #"<<endl; //#5
cout <<"  #                                                          BORDER      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                          ISTER FORT                  #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX DWARF                        #"<<endl; //@
cout <<"  #                                   XXXXX BORDER                       #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14
getch();
}
{ // HUMAN KINGDOM 1    (1, 1)  PLAINS      ( q,q)  D De LH
    qq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #@@@@@                         XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #@@@@@                         XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #@@@@@                         XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14
locationhistory="qq";
ch7=getch();


if (ch7==119) // up
goto qq;
if (ch7==100) // right
goto wq;
if (ch7==115) // down
goto qw;
if (ch7==97)  // left
goto qq;

goto qq;

}
{ // HUMAN KINGDOM 2    (2, 1)  PLAINS      ( w,q)  D De LH
    wq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #     @@@@@                    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #     @@@@@                    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #     @@@@@                    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14
locationhistory="wq";
ch7=getch();


if (ch7==119) // up
goto wq;
if (ch7==100) // right
goto eq;
if (ch7==115) // down
goto ww;
if (ch7==97)  // left
goto qq;

goto qq;
}
{ // HUMAN KINGDOM 3    (3, 1)  PLAINS      ( e,q)  D De LH
    eq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #          @@@@@               XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #          @@@@@               XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #          @@@@@               XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14
locationhistory="eq";
ch7=getch();



if (ch7==119) // up
goto eq;
if (ch7==100) // right
goto rq;
if (ch7==115) // down
goto ew;
if (ch7==97)  // left
goto wq;

goto eq;
}
{ // HUMAN KINGDOM 4    (4, 1)  PLAINS      ( r,q)  D De LH
    rq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #               @@@@@          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #               @@@@@          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #               @@@@@          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="rq";


if (ch7==119)  // up
goto rq;
if (ch7==100)  // right
goto tq;
if (ch7==115)  // down
goto rw;
if (ch7==97)   // left
goto eq;

goto rq;
}
{ // HUMAN KINGDOM 5    (5, 1)  PLAINS      ( t,q)  D De LH
    tq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                    @@@@@     XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                    @@@@@     XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                    @@@@@     XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="tq";

if (ch7==119)  // up
goto tq;
if (ch7==100)  // right
goto yq;
if (ch7==115)  // down
goto tw;
if (ch7==97)   // left
goto rq;

goto tq;
}
{ // HUMAN KINGDOM 6    (6, 1)  PLAINS      ( y,q)  D De LH
    yq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                         @@@@@XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                         @@@@@XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                         @@@@@XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="yq";

if (ch7==119)  // up
goto yq;
if (ch7==100)  // right
goto uq;
if (ch7==115)  // down
goto yw;
if (ch7==97)   // left
goto tq;

goto yq;
}
{ // HUMAN KINGDOM 7    (7, 1)  OAK FOREST  ( u,q)  D De LH
    uq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #  OAK FOREST   #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              @@@@@XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              @@@@@XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              @@@@@XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="uq";

if (ch7==119)  // up
goto uq;
if (ch7==100)  // right
goto iq;
if (ch7==115)  // down
goto uw;
if (ch7==97)   // left
goto yq;

goto uq;
}
{ // HUMAN KINGDOM 8    (8, 1)  OAK FOREST  ( i,q)  D De LH
    iq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXX@@@@@XXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXX@@@@@XXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXX@@@@@XXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="iq";

if (ch7==119)  // up
goto iq;
if (ch7==100)  // right
goto oq;
if (ch7==115)  // down
goto iw;
if (ch7==97)   // left
goto uq;

goto iq;
}
{ // HUMAN KINGDOM 9    (9, 1)  OAK FOREST  ( o,q)  D De LH
    oq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXX@@@@@XXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXX@@@@@XXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXX@@@@@XXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="oq";

if (ch7==119)  // up
goto oq;
if (ch7==100)  // right
goto qpq;
if (ch7==115)  // down
goto ow;
if (ch7==97)   // left
goto iq;

goto oq;
}
{ // HUMAN KINGDOM 10   (10,1)  OAK FOREST  (qp,q)  D De LH
    qpq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXX@@@@@XXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXX@@@@@XXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXX@@@@@XXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qpq";

if (ch7==119)  // up
goto qpq;
if (ch7==100)  // right
goto qqq;
if (ch7==115)  // down
goto qpw;
if (ch7==97)   // left
goto oq;

goto qpq;
}
{ // HUMAN KINGDOM 11   (11,1)  OAK FOREST  (qq,q)  D De LH
    qqq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXX@@@@@XXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXX@@@@@XXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXX@@@@@XXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qqq";

if (ch7==119)  // up
goto qqq;
if (ch7==100)  // right
goto qwq;
if (ch7==115)  // down
goto qqw;
if (ch7==97)   // left
goto qpq;

goto qqq;
}
{ // HUMAN KINGDOM 12   (12,1)  OAK FOREST  (qw,q)  D De LH
    qwq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXX@@@@@XXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXX@@@@@XXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXX@@@@@XXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qwq";

if (ch7==119)  // up
goto qwq;
if (ch7==100)  // right
goto qeq;
if (ch7==115)  // down
goto qww;
if (ch7==97)   // left
goto qqq;

goto qwq;
}
{ // HUMAN KINGDOM 13   (13,1)  OAK FOREST  (qe,q)  D De LH
    qeq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX@@@@@     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX@@@@@     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX@@@@@     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qeq";

if (ch7==119)  // up
goto qeq;
if (ch7==100)  // right
goto qrq;
if (ch7==115)  // down
goto qew;
if (ch7==97)   // left
goto qwq;

goto qeq;
}
{ // HUMAN KINGDOM 14   (14,1)  PLAINS      (qr,q)  D De LH
    qrq:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX@@@@@#"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX@@@@@#"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX@@@@@#"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qrq";

if (ch7==119)  // up
goto qrq;
if (ch7==100)  // right
goto qrq;
if (ch7==115)  // down
goto qrw;
if (ch7==97)   // left
goto qeq;

goto qrq;
}
{ // HUMAN KINGDOM 15   (1, 2)  PLAINS      ( q,w)  D De LH
    qw:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #@@@@@                              XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #@@@@@                              XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #@@@@@                              XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qw";

if (ch7==119) // up
goto qq;
if (ch7==100) // right
goto ww;
if (ch7==115) // down
goto qe;
if (ch7==97)  // left
goto qw;

goto qw;
}
{ // HUMAN KINGDOM 16   (2, 2)  PLAINS      ( w,w)  D De LH
    ww:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #     @@@@@                         XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #     @@@@@                         XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #     @@@@@                         XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ww";

if (ch7==119) // up
goto wq;
if (ch7==100) // right
goto ew;
if (ch7==115) // down
goto we;
if (ch7==97)  // left
goto qw;

goto ww;
}
{ // HUMAN KINGDOM 17   (3, 2)  PLAINS      ( e,w)  D De LH
    ew:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #          @@@@@                    XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          @@@@@                    XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #          @@@@@                    XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ew";

if (ch7==119) // up
goto eq;
if (ch7==100) // right
goto rw;
if (ch7==115) // down
goto ee;
if (ch7==97)  // left
goto ww;

goto ew;
}
{ // HUMAN KINGDOM 18   (4, 2)  PLAINS      ( r,w)  D De LH
    rw:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #               @@@@@               XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #               @@@@@               XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #               @@@@@               XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="rw";

if (ch7==119) // up
goto rq;
if (ch7==100) // right
goto tw;
if (ch7==115) // down
goto re;
if (ch7==97)  // left
goto ew;

goto rw;
}
{ // HUMAN KINGDOM 19   (5, 2)  PLAINS      ( t,w)  D De LH
    tw:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                    @@@@@          XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                    @@@@@          XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                    @@@@@          XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="tw";

if (ch7==119) // up
goto tq;
if (ch7==100) // right
goto yw;
if (ch7==115) // down
goto te;
if (ch7==97)  // left
goto rw;

goto tw;
}
{ // HUMAN KINGDOM 20   (6, 2)  PLAINS      ( y,w)  D De LH
    yw:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                         @@@@@     XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                         @@@@@     XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                         @@@@@     XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="yw";

if (ch7==119) // up
goto yq;
if (ch7==100) // right
goto uw;
if (ch7==115) // down
goto ye;
if (ch7==97)  // left
goto tw;

goto yw;
}
{ // HUMAN KINGDOM 21   (7, 2)  PLAINS      ( u,w)  D De LH
    uw:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              @@@@@XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                              @@@@@XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                              @@@@@XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="uw";

if (ch7==119) // up
goto uq;
if (ch7==100) // right
goto iw;
if (ch7==115) // down
goto ue;
if (ch7==97)  // left
goto yw;

goto uw;
}
{ // HUMAN KINGDOM 22   (8, 2)  OAK FOREST  ( i,w)  D De LH
    iw:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   @@@@@XXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   @@@@@XXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   @@@@@XXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="iw";

if (ch7==119) // up
goto iq;
if (ch7==100) // right
goto ow;
if (ch7==115) // down
goto ie;
if (ch7==97)  // left
goto uw;

goto iw;
}
{ // HUMAN KINGDOM 23   (9, 2)  OAK FOREST  ( o,w)  D De LH
    ow:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXX@@@@@XXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXX@@@@@XXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXX@@@@@XXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ow";

if (ch7==119) // up
goto oq;
if (ch7==100) // right
goto qpw;
if (ch7==115) // down
goto oe;
if (ch7==97)  // left
goto iw;

goto qq;
}
{ // HUMAN KINGDOM 24   (10,2)  TEMPLE      (qp,w)  D De LH
    qpw:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #     TEMPLE    #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXX@@@@@XXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXX@@@@@XXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXX@@@@@XXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qpw";

if (ch7==119) // up
goto qpq;
if (ch7==100) // right
goto qqw;
if (ch7==115) // down
goto qpe;
if (ch7==97)  // left
goto ow;

goto qq;
}
{ // HUMAN KINGDOM 25   (11,2)  OAK FOREST  (qq,w)  D De LH
    qqw:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXX@@@@@XXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXX@@@@@XXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXX@@@@@XXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qqw";

if (ch7==119) // up
goto qqq;
if (ch7==100) // right
goto qww;
if (ch7==115) // down
goto qqe;
if (ch7==97)  // left
goto qpw;

goto qqw;
}
{ // HUMAN KINGDOM 26   (12,2)  OAK FOREST  (qw,w)  D De LH
    qww:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXX@@@@@          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXX@@@@@          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXX@@@@@          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qww";

if (ch7==119) // up
goto qwq;
if (ch7==100) // right
goto qew;
if (ch7==115) // down
goto qwe;
if (ch7==97)  // left
goto qqw;

goto qww;
}
{ // HUMAN KINGDOM 27   (13,2)  PLAINS      (qe,w)  D De LH
    qew:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX@@@@@     #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX@@@@@     #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX@@@@@     #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qew";

if (ch7==119) // up
goto qeq;
if (ch7==100) // right
goto qrw;
if (ch7==115) // down
goto qee;
if (ch7==97)  // left
goto qww;

goto qew;
}
{ // HUMAN KINGDOM 28   (14,2)  PLAINS      (qr,w)  D De LH
    qrw:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX     @@@@@#"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX     @@@@@#"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX     @@@@@#"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qrw";

if (ch7==119) // up
goto qrq;
if (ch7==100) // right
goto qrw;
if (ch7==115) // down
goto qre;
if (ch7==97)  // left
goto qew;

goto qrw;
}
{ // HUMAN KINGDOM 29   (1 ,3)  PLAINS      (qq,e)  D De LH
    qe:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #@@@@@     XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #@@@@@     XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #@@@@@     XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qe";

if (ch7==119) // up
goto qw;
if (ch7==100) // right
goto we;
if (ch7==115) // down
goto qr;
if (ch7==97)  // left
goto qe;

goto qe;
}
{ // HUMAN KINGDOM 30   (2 ,3)  PLAINS      (qq,e)  D De LH
    we:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #     @@@@@XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #     @@@@@XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #     @@@@@XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="we";

if (ch7==119) // up
goto ww;
if (ch7==100) // right
goto ee;
if (ch7==115) // down
goto wr;
if (ch7==97)  // left
goto qe;

goto we;
}
{ // HUMAN KINGDOM 31   (3 ,3)  CITADEL     (qq,e)  D De LH    #
    ee:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #     CITADEL   #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          @@@@@XXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          @@@@@XXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          @@@@@XXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ee";

if (ch7==119) // up
goto ew;
if (ch7==100) // right
goto re;
if (ch7==115) // down
goto er;
if (ch7==97)  // left
goto we;

goto ee;
}
{ // HUMAN KINGDOM 32   (4 ,3)  CITADEL     (qq,e)  D De LH    #
    re:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #     CITADEL   #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXX@@@@@                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXX@@@@@                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXX@@@@@                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="re";

if (ch7==119) // up
goto rw;
if (ch7==100) // right
goto te;
if (ch7==115) // down
goto rr;
if (ch7==97)  // left
goto ee;

goto re;
}
{ // HUMAN KINGDOM 33   (5 ,3)  PLAINS      (qq,e)  D De LH
    te:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX@@@@@               XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX@@@@@               XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX@@@@@               XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="te";

if (ch7==119) // up
goto tw;
if (ch7==100) // right
goto ye;
if (ch7==115) // down
goto tr;
if (ch7==97)  // left
goto re;

goto te;
}
{ // HUMAN KINGDOM 34   (6 ,3)  PLAINS      (qq,e)  D De LH
    ye:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX     @@@@@          XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX     @@@@@          XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX     @@@@@          XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ye";

if (ch7==119) // up
goto yw;
if (ch7==100) // right
goto ue;
if (ch7==115) // down
goto yr;
if (ch7==97)  // left
goto te;

goto ye;
}
{ // HUMAN KINGDOM 35   (7 ,3)  PLAINS      (qq,e)  D De LH
    ue:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX          @@@@@     XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX          @@@@@     XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX          @@@@@     XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ue";

if (ch7==119) // up
goto uw;
if (ch7==100) // right
goto ie;
if (ch7==115) // down
goto ur;
if (ch7==97)  // left
goto ye;

goto ue;
}
{ // HUMAN KINGDOM 36   (8 ,3)  PLAINS      (qq,e)  D De LH
    ie:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX               @@@@@XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX               @@@@@XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX               @@@@@XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ie";

if (ch7==119) // up
goto iw;
if (ch7==100) // right
goto oe;
if (ch7==115) // down
goto ir;
if (ch7==97)  // left
goto ue;

goto ie;
}
{ // HUMAN KINGDOM 37   (9 ,3)  OAK FOREST  (qq,e)  D De LH
    oe:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    @@@@@XXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    @@@@@XXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    @@@@@XXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="oe";

if (ch7==119) // up
goto ow;
if (ch7==100) // right
goto qpe;
if (ch7==115) // down
goto orr;
if (ch7==97)  // left
goto ie;

goto oe;
}
{ // HUMAN KINGDOM 38   (10,3)  OAK FOREST  (qq,e)  D De LH
    qpe:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXX@@@@@XXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXX@@@@@XXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXX@@@@@XXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qpe";

if (ch7==119) // up
goto qpw;
if (ch7==100) // right
goto qqe;
if (ch7==115) // down
goto qpr;
if (ch7==97)  // left
goto oe;

goto qpe;
}
{ // HUMAN KINGDOM 39   (11,3)  OAK FOREST  (qq,e)  D De LH
    qqe:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXX@@@@@XXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXX@@@@@XXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXX@@@@@XXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qqe";

if (ch7==119) // up
goto qqw;
if (ch7==100) // right
goto qwe;
if (ch7==115) // down
goto qqr;
if (ch7==97)  // left
goto qpe;

goto qqe;
}
{ // HUMAN KINGDOM 40   (12,3)  OAK FOREST  (qq,e)  D De LH
    qwe:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   OAK FOREST  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXX@@@@@          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXX@@@@@          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXX@@@@@          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qwe";

if (ch7==119) // up
goto qww;
if (ch7==100) // right
goto qee;
if (ch7==115) // down
goto qwr;
if (ch7==97)  // left
goto qqe;

goto qwe;
}
{ // HUMAN KINGDOM 41   (13,3)  PLAINS      (qq,e)  D De LH
    qee:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX@@@@@     #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX@@@@@     #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX@@@@@     #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qee";

if (ch7==119) // up
goto qew;
if (ch7==100) // right
goto qre;
if (ch7==115) // down
goto qer;
if (ch7==97)  // left
goto qwe;

goto qee;
}
{ // HUMAN KINGDOM 42   (14,3)  PLAINS      (qq,e)  D De LH
    qre:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX     @@@@@#"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX     @@@@@#"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX     @@@@@#"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qre";

if (ch7==119) // up
goto qrw;
if (ch7==100) // right
goto qre;
if (ch7==115) // down
goto qrr;
if (ch7==97)  // left
goto qee;

goto qre;
}
{ // HUMAN KINGDOM 43   (1 ,4)  PLAINS      (qq,r)  D De LH
    qr:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #@@@@@     XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #@@@@@     XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #@@@@@     XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qr";

if (ch7==119) // up
goto qe;
if (ch7==100) // right
goto wr;
if (ch7==115) // down
goto qt;
if (ch7==97)  // left
goto qr;

goto qr;
}
{ // HUMAN KINGDOM 44   (2 ,4)  PLAINS      (qq,r)  D De LH
    wr:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #     @@@@@XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #     @@@@@XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #     @@@@@XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="wr";

if (ch7==119) // up
goto we;
if (ch7==100) // right
goto er;
if (ch7==115) // down
goto wt;
if (ch7==97)  // left
goto qr;

goto wr;
}
{ // HUMAN KINGDOM 45   (3 ,4)  CITADEL     (qq,r)  D De LH    #
    er:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #     CITADEL   #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          @@@@@XXXXX                                                  #"<<endl; //@
cout <<"  #          @@@@@XXXXX                                                  #"<<endl; //@4
cout <<"  #          @@@@@XXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="er";

if (ch7==119) // up
goto ee;
if (ch7==100) // right
goto rr;
if (ch7==115) // down
goto et;
if (ch7==97)  // left
goto wr;

goto er;
}
{ // HUMAN KINGDOM 46   (4 ,4)  CITADEL     (qq,r)  D De LH    #
    rr:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #     CITADEL   #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXX@@@@@                                                  #"<<endl; //@
cout <<"  #          XXXXX@@@@@                                                  #"<<endl; //@4
cout <<"  #          XXXXX@@@@@                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="rr";

if (ch7==119) // up
goto re;
if (ch7==100) // right
goto tr;
if (ch7==115) // down
goto rt;
if (ch7==97)  // left
goto er;

goto rr;
}
{ // HUMAN KINGDOM 47   (5 ,4)  PLAINS      (qq,r)  D De LH
    tr:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX@@@@@                                             #"<<endl; //@
cout <<"  #          XXXXXXXXXX@@@@@                                             #"<<endl; //@4
cout <<"  #          XXXXXXXXXX@@@@@                                             #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="tr";

if (ch7==119) // up
goto te;
if (ch7==100) // right
goto yr;
if (ch7==115) // down
goto tt;
if (ch7==97)  // left
goto rr;

goto tr;
}
{ // HUMAN KINGDOM 48   (6 ,4)  PLAINS      (qq,r)  D De LH
    yr:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX     @@@@@                                        #"<<endl; //@
cout <<"  #          XXXXXXXXXX     @@@@@                                        #"<<endl; //@4
cout <<"  #          XXXXXXXXXX     @@@@@                                        #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="yr";

if (ch7==119) // up
goto ye;
if (ch7==100) // right
goto ur;
if (ch7==115) // down
goto yt;
if (ch7==97)  // left
goto tr;

goto yr;
}
{ // HUMAN KINGDOM 49   (7 ,4)  PLAINS      (qq,r)  D De LH
    ur:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX          @@@@@                                   #"<<endl; //@
cout <<"  #          XXXXXXXXXX          @@@@@                                   #"<<endl; //@4
cout <<"  #          XXXXXXXXXX          @@@@@                                   #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ur";

if (ch7==119) // up
goto ue;
if (ch7==100) // right
goto ir;
if (ch7==115) // down
goto ut;
if (ch7==97)  // left
goto yr;

goto ur;
}
{ // HUMAN KINGDOM 50   (8 ,4)  PLAINS      (qq,r)  D De LH
    ir:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX               @@@@@                              #"<<endl; //@
cout <<"  #          XXXXXXXXXX               @@@@@                              #"<<endl; //@4
cout <<"  #          XXXXXXXXXX               @@@@@                              #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ir";

if (ch7==119) // up
goto ie;
if (ch7==100) // right
goto orr;
if (ch7==115) // down
goto it;
if (ch7==97)  // left
goto ur;

goto ir;
}
{ // HUMAN KINGDOM 51   (9 ,4)  PLAINS      (qq,r)  D De LH
    orr:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    @@@@@                         #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    @@@@@                         #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                    @@@@@                         #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="orr";

if (ch7==119) // up
goto oe;
if (ch7==100) // right
goto qpr;
if (ch7==115) // down
goto ot;
if (ch7==97)  // left
goto ir;

goto orr;
}
{ // HUMAN KINGDOM 52   (10,4)  PLAINS      (qq,r)  D De LH
    qpr:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                         @@@@@                    #"<<endl; //@
cout <<"  #          XXXXXXXXXX                         @@@@@                    #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                         @@@@@                    #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qpr";

if (ch7==119) // up
goto qpe;
if (ch7==100) // right
goto qqr;
if (ch7==115) // down
goto qpt;
if (ch7==97)  // left
goto orr;

goto qpr;
}
{ // HUMAN KINGDOM 53   (11,4)  PLAINS      (qq,r)  D De LH
    qqr:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                              @@@@@               #"<<endl; //@
cout <<"  #          XXXXXXXXXX                              @@@@@               #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                              @@@@@               #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qqr";

if (ch7==119) // up
goto qqe;
if (ch7==100) // right
goto qwr;
if (ch7==115) // down
goto qqt;
if (ch7==97)  // left
goto qpr;

goto qqr;
}
{ // HUMAN KINGDOM 54   (12,4)  PLAINS      (qq,r)  D De LH
    qwr:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                   @@@@@          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                   @@@@@          #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                   @@@@@          #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qwr";

if (ch7==119) // up
goto qwe;
if (ch7==100) // right
goto qer;
if (ch7==115) // down
goto qwt;
if (ch7==97)  // left
goto qqr;

goto qwr;
}
{ // HUMAN KINGDOM 55   (13,4)  PLAINS      (qq,r)  D De LH
    qer:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                        @@@@@     #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                        @@@@@     #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                        @@@@@     #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qer";

if (ch7==119) // up
goto qee;
if (ch7==100) // right
goto qrr;
if (ch7==115) // down
goto qet;
if (ch7==97)  // left
goto qwr;

goto qer;
}
{ // HUMAN KINGDOM 56   (14,4)  PLAINS      (qq,r)  D De LH
    qrr:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                             @@@@@#"<<endl; //@
cout <<"  #          XXXXXXXXXX                                             @@@@@#"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                             @@@@@#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qrr";

if (ch7==119) // up
goto qre;
if (ch7==100) // right
goto qrr;
if (ch7==115) // down
goto qrt;
if (ch7==97)  // left
goto qer;

goto qrr;
}
{ // HUMAN KINGDOM 57   (1 ,5)  PLAINS      (qq,t)  D De LH
    qt:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #@@@@@                                                                 #"<<endl; //#
cout <<"  #@@@@@                                                                 #"<<endl; //#5
cout <<"  #@@@@@                                                                 #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qt";

if (ch7==119) // up
goto qr;
if (ch7==100) // right
goto wt;
if (ch7==115) // down
goto qy;
if (ch7==97)  // left
goto qt;

goto qt;
}
{ // HUMAN KINGDOM 58   (2 ,5)  PLAINS      (qq,t)  D De LH
    wt:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #     @@@@@                                                            #"<<endl; //#
cout <<"  #     @@@@@                                                            #"<<endl; //#5
cout <<"  #     @@@@@                                                            #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="wt";

if (ch7==119) // up
goto wr;
if (ch7==100) // right
goto et;
if (ch7==115) // down
goto wy;
if (ch7==97)  // left
goto qt;

goto wt;
}
{ // HUMAN KINGDOM 59   (3 ,5)  PLAINS      (qq,t)  D De LH
    et:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          @@@@@                                                       #"<<endl; //#
cout <<"  #          @@@@@                                                       #"<<endl; //#5
cout <<"  #          @@@@@                                                       #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="et";

if (ch7==119) // up
goto er;
if (ch7==100) // right
goto rt;
if (ch7==115) // down
goto ey;
if (ch7==97)  // left
goto wt;

goto et;
}
{ // HUMAN KINGDOM 60   (4 ,5)  PLAINS      (qq,t)  D De LH
    rt:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #               @@@@@                                                  #"<<endl; //#
cout <<"  #               @@@@@                                                  #"<<endl; //#5
cout <<"  #               @@@@@                                                  #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="rt";

if (ch7==119) // up
goto rr;
if (ch7==100) // right
goto tt;
if (ch7==115) // down
goto ry;
if (ch7==97)  // left
goto et;

goto rt;
}
{ // HUMAN KINGDOM 61   (5 ,5)  PLAINS      (qq,t)  D De LH
    tt:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                    @@@@@                                             #"<<endl; //#
cout <<"  #                    @@@@@                                             #"<<endl; //#5
cout <<"  #                    @@@@@                                             #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="tt";

if (ch7==119) // up
goto tr;
if (ch7==100) // right
goto yt;
if (ch7==115) // down
goto ty;
if (ch7==97)  // left
goto rt;

goto tt;
}
{ // HUMAN KINGDOM 62   (6 ,5)  PLAINS      (qq,t)  D De LH
    yt:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                         @@@@@                                        #"<<endl; //#
cout <<"  #                         @@@@@                                        #"<<endl; //#5
cout <<"  #                         @@@@@                                        #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="yt";

if (ch7==119) // up
goto yr;
if (ch7==100) // right
goto ut;
if (ch7==115) // down
goto yy;
if (ch7==97)  // left
goto tt;

goto yt;
}
{ // HUMAN KINGDOM 63   (7 ,5)  PLAINS      (qq,t)  D De LH
    ut:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                              @@@@@                                   #"<<endl; //#
cout <<"  #                              @@@@@                                   #"<<endl; //#5
cout <<"  #                              @@@@@                                   #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ut";

if (ch7==119) // up
goto ur;
if (ch7==100) // right
goto it;
if (ch7==115) // down
goto uy;
if (ch7==97)  // left
goto yt;

goto ut;
}
{ // HUMAN KINGDOM 64   (8 ,5)  PLAINS      (qq,t)  D De LH
    it:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                   @@@@@                              #"<<endl; //#
cout <<"  #                                   @@@@@                              #"<<endl; //#5
cout <<"  #                                   @@@@@                              #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="it";

if (ch7==119) // up
goto ir;
if (ch7==100) // right
goto ot;
if (ch7==115) // down
goto iy;
if (ch7==97)  // left
goto ut;

goto it;
}
{ // HUMAN KINGDOM 65   (9 ,5)  PLAINS      (qq,t)  D De LH
    ot:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                        @@@@@                         #"<<endl; //#
cout <<"  #                                        @@@@@                         #"<<endl; //#5
cout <<"  #                                        @@@@@                         #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ot";

if (ch7==119) // up
goto orr;
if (ch7==100) // right
goto qpt;
if (ch7==115) // down
goto oy;
if (ch7==97)  // left
goto it;

goto ot;
}
{ // HUMAN KINGDOM 66   (10,5)  PLAINS      (qq,t)  D De LH
    qpt:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                             @@@@@                    #"<<endl; //#
cout <<"  #                                             @@@@@                    #"<<endl; //#5
cout <<"  #                                             @@@@@                    #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qpt";

if (ch7==119) // up
goto qpr;
if (ch7==100) // right
goto qqt;
if (ch7==115) // down
goto qpy;
if (ch7==97)  // left
goto ot;

goto qpt;
}
{ // HUMAN KINGDOM 67   (11,5)  PLAINS      (qq,t)  D De LH
    qqt:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                  @@@@@               #"<<endl; //#
cout <<"  #                                                  @@@@@               #"<<endl; //#5
cout <<"  #                                                  @@@@@               #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qqt";

if (ch7==119) // up
goto qqr;
if (ch7==100) // right
goto qwt;
if (ch7==115) // down
goto qqy;
if (ch7==97)  // left
goto qpt;

goto qqt;
}
{ // HUMAN KINGDOM 68   (12,5)  PLAINS      (qq,t)  D De LH
    qwt:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                       @@@@@          #"<<endl; //#
cout <<"  #                                                       @@@@@          #"<<endl; //#5
cout <<"  #                                                       @@@@@          #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qwt";

if (ch7==119) // up
goto qwr;
if (ch7==100) // right
goto qet;
if (ch7==115) // down
goto qwy;
if (ch7==97)  // left
goto qqt;

goto qwt;
}
{ // HUMAN KINGDOM 69   (13,5)  PLAINS      (qq,t)  D De LH
    qet:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                            @@@@@     #"<<endl; //#
cout <<"  #                                                            @@@@@     #"<<endl; //#5
cout <<"  #                                                            @@@@@     #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qet";

if (ch7==119) // up
goto qer;
if (ch7==100) // right
goto qrt;
if (ch7==115) // down
goto qey;
if (ch7==97)  // left
goto qwt;

goto qet;
}
{ // HUMAN KINGDOM 70   (14,5)  PLAINS      (qq,t)  D De LH
    qrt:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                 @@@@@#"<<endl; //#
cout <<"  #                                                                 @@@@@#"<<endl; //#5
cout <<"  #                                                                 @@@@@#"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qrt";

if (ch7==119) // up
goto qrr;
if (ch7==100) // right
goto qrt;
if (ch7==115) // down
goto qry;
if (ch7==97)  // left
goto qet;

goto qrt;
}
{ // HUMAN KINGDOM 71   (1 ,6)  PLAINS      (qq,y)  D De LH
    qy:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #@@@@@                                        XXXXX               XXXXX#"<<endl; //@
cout <<"  #@@@@@                                        XXXXX               XXXXX#"<<endl; //@6
cout <<"  #@@@@@                                        XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qy";

if (ch7==119) // up
goto qt;
if (ch7==100) // right
goto wy;
if (ch7==115) // down
goto qu;
if (ch7==97)  // left
goto qy;

goto qy;
}
{ // HUMAN KINGDOM 72   (2 ,6)  PLAINS      (qq,y)  D De LH
    wy:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #     @@@@@                                   XXXXX               XXXXX#"<<endl; //@
cout <<"  #     @@@@@                                   XXXXX               XXXXX#"<<endl; //@6
cout <<"  #     @@@@@                                   XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="wy";

if (ch7==119) // up
goto wt;
if (ch7==100) // right
goto ey;
if (ch7==115) // down
goto wu;
if (ch7==97)  // left
goto qy;

goto wy;
}
{ // HUMAN KINGDOM 73   (3 ,6)  PLAINS      (qq,y)  D De LH
    ey:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #          @@@@@                              XXXXX               XXXXX#"<<endl; //@
cout <<"  #          @@@@@                              XXXXX               XXXXX#"<<endl; //@6
cout <<"  #          @@@@@                              XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ey";

if (ch7==119) // up
goto et;
if (ch7==100) // right
goto ry;
if (ch7==115) // down
goto eu;
if (ch7==97)  // left
goto wy;

goto ey;
}
{ // HUMAN KINGDOM 74   (4 ,6)  PLAINS      (qq,y)  D De LH
    ry:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #               @@@@@                         XXXXX               XXXXX#"<<endl; //@
cout <<"  #               @@@@@                         XXXXX               XXXXX#"<<endl; //@6
cout <<"  #               @@@@@                         XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ry";

if (ch7==119) // up
goto rt;
if (ch7==100) // right
goto ty;
if (ch7==115) // down
goto ru;
if (ch7==97)  // left
goto ey;

goto ry;
}
{ // HUMAN KINGDOM 75   (5 ,6)  PLAINS      (qq,y)  D De LH
    ty:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                    @@@@@                    XXXXX               XXXXX#"<<endl; //@
cout <<"  #                    @@@@@                    XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                    @@@@@                    XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ty";

if (ch7==119) // up
goto tt;
if (ch7==100) // right
goto yy;
if (ch7==115) // down
goto tu;
if (ch7==97)  // left
goto ry;

goto ty;
}
{ // HUMAN KINGDOM 76   (6 ,6)  PLAINS      (qq,y)  D De LH
    yy:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                         @@@@@               XXXXX               XXXXX#"<<endl; //@
cout <<"  #                         @@@@@               XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                         @@@@@               XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="yy";

if (ch7==119) // up
goto yt;
if (ch7==100) // right
goto uy;
if (ch7==115) // down
goto yu;
if (ch7==97)  // left
goto ty;

goto yy;
}
{ // HUMAN KINGDOM 77   (7 ,6)  PLAINS      (qq,y)  D De LH
    uy:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                              @@@@@          XXXXX               XXXXX#"<<endl; //@
cout <<"  #                              @@@@@          XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                              @@@@@          XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="uy";

if (ch7==119) // up
goto ut;
if (ch7==100) // right
goto iy;
if (ch7==115) // down
goto uu;
if (ch7==97)  // left
goto yy;

goto uy;
}
{ // HUMAN KINGDOM 78   (8 ,6)  PLAINS      (qq,y)  D De LH
    iy:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   @@@@@     XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                   @@@@@     XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                   @@@@@     XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="iy";

if (ch7==119) // up
goto it;
if (ch7==100) // right
goto oy;
if (ch7==115) // down
goto iu;
if (ch7==97)  // left
goto uy;

goto iy;
}
{ // HUMAN KINGDOM 79   (9 ,6)  PLAINS      (qq,y)  D De LH
    oy:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                        @@@@@XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                        @@@@@XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                        @@@@@XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="oy";

if (ch7==119) // up
goto ot;
if (ch7==100) // right
goto qpy;
if (ch7==115) // down
goto ou;
if (ch7==97)  // left
goto iy;

goto oy;
}
{ // HUMAN KINGDOM 80   (10,6)  ISTER FORT  (qq,y)  D De LH
    qpy:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   ISTER FORT  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             @@@@@               XXXXX#"<<endl; //@
cout <<"  #                                             @@@@@               XXXXX#"<<endl; //@6
cout <<"  #                                             @@@@@               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qpy";

if (ch7==119) // up
goto qpt;
if (ch7==100) // right
goto qqy;
if (ch7==115) // down
goto qpu;
if (ch7==97)  // left
goto oy;

goto qpy;
}
{ // HUMAN KINGDOM 81   (11,6)  PLAINS      (qq,y)  D De LH
    qqy:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX@@@@@          XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX@@@@@          XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX@@@@@          XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qqy";

if (ch7==119) // up
goto qqt;
if (ch7==100) // right
goto qwy;
if (ch7==115) // down
goto qqu;
if (ch7==97)  // left
goto qpy;

goto qqy;
}
{ // HUMAN KINGDOM 82   (12,6)  PLAINS      (qq,y)  D De LH
    qwy:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX     @@@@@     XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX     @@@@@     XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX     @@@@@     XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qwy";

if (ch7==119) // up
goto qwt;
if (ch7==100) // right
goto qey;
if (ch7==115) // down
goto qwu;
if (ch7==97)  // left
goto qqy;

goto qwy;
}
{ // HUMAN KINGDOM 83   (13,6)  PLAINS      (qq,y)  D De LH
    qey:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX          @@@@@XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX          @@@@@XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX          @@@@@XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qey";

if (ch7==119) // up
goto qet;
if (ch7==100) // right
goto qry;
if (ch7==115) // down
goto qeu;
if (ch7==97)  // left
goto qwy;

goto qey;
}
{ // HUMAN KINGDOM 84   (14,6)  ELF BORDER  (qq,y)  D De LH
    qry:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #   ELF BORDER  #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               @@@@@#"<<endl; //@
cout <<"  #                                             XXXXX               @@@@@#"<<endl; //@6
cout <<"  #                                             XXXXX               @@@@@#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qry";

if (ch7==119) // up
goto qrt;
if (ch7==100) // right
goto qry;
if (ch7==115) // down
goto qru;
if (ch7==97)  // left
goto qey;

goto qry;
}
{ // HUMAN KINGDOM 85   (1 ,7)  PLAINS      (qq,u)  D De LH
    qu:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #@@@@@                                                                 #"<<endl; //#
cout <<"  #@@@@@                                                                 #"<<endl; //#7
cout <<"  #@@@@@                                                                 #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qu";

if (ch7==119) // up
goto qy;
if (ch7==100) // right
goto wu;
if (ch7==115) // down
goto qi;
if (ch7==97)  // left
goto qu;

goto qu;
}
{ // HUMAN KINGDOM 86   (2 ,7)  PLAINS      (qq,u)  D De LH
    wu:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #     @@@@@                                                            #"<<endl; //#
cout <<"  #     @@@@@                                                            #"<<endl; //#7
cout <<"  #     @@@@@                                                            #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="wu";

if (ch7==119) // up
goto wy;
if (ch7==100) // right
goto eu;
if (ch7==115) // down
goto wi;
if (ch7==97)  // left
goto qu;

goto wu;
}
{ // HUMAN KINGDOM 87   (3 ,7)  PLAINS      (qq,u)  D De LH
    eu:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #          @@@@@                                                       #"<<endl; //#
cout <<"  #          @@@@@                                                       #"<<endl; //#7
cout <<"  #          @@@@@                                                       #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="eu";

if (ch7==119) // up
goto ey;
if (ch7==100) // right
goto ru;
if (ch7==115) // down
goto ei;
if (ch7==97)  // left
goto wu;

goto eu;
}
{ // HUMAN KINGDOM 88   (4 ,7)  PLAINS      (qq,u)  D De LH
    ru:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #               @@@@@                                                  #"<<endl; //#
cout <<"  #               @@@@@                                                  #"<<endl; //#7
cout <<"  #               @@@@@                                                  #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ru";

if (ch7==119) // up
goto ry;
if (ch7==100) // right
goto tu;
if (ch7==115) // down
goto ri;
if (ch7==97)  // left
goto eu;

goto ru;
}
{ // HUMAN KINGDOM 89   (5 ,7)  PLAINS      (qq,u)  D De LH
    tu:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                    @@@@@                                             #"<<endl; //#
cout <<"  #                    @@@@@                                             #"<<endl; //#7
cout <<"  #                    @@@@@                                             #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="tu";

if (ch7==119) // up
goto ty;
if (ch7==100) // right
goto yu;
if (ch7==115) // down
goto ti;
if (ch7==97)  // left
goto ru;

goto tu;
}
{ // HUMAN KINGDOM 90   (6 ,7)  PLAINS      (qq,u)  D De LH
    yu:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                         @@@@@                                        #"<<endl; //#
cout <<"  #                         @@@@@                                        #"<<endl; //#7
cout <<"  #                         @@@@@                                        #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="yu";

if (ch7==119) // up
goto yy;
if (ch7==100) // right
goto uu;
if (ch7==115) // down
goto yi;
if (ch7==97)  // left
goto tu;

goto yu;
}
{ // HUMAN KINGDOM 91   (7 ,7)  PLAINS      (qq,u)  D De LH
    uu:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                              @@@@@                                   #"<<endl; //#
cout <<"  #                              @@@@@                                   #"<<endl; //#7
cout <<"  #                              @@@@@                                   #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="uu";

if (ch7==119) // up
goto uy;
if (ch7==100) // right
goto iu;
if (ch7==115) // down
goto ui;
if (ch7==97)  // left
goto yu;

goto uu;
}
{ // HUMAN KINGDOM 92   (8 ,7)  PLAINS      (qq,u)  D De LH
    iu:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                   @@@@@                              #"<<endl; //#
cout <<"  #                                   @@@@@                              #"<<endl; //#7
cout <<"  #                                   @@@@@                              #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="iu";

if (ch7==119) // up
goto iy;
if (ch7==100) // right
goto ou;
if (ch7==115) // down
goto ii;
if (ch7==97)  // left
goto uu;

goto iu;
}
{ // HUMAN KINGDOM 93   (9 ,7)  PLAINS      (qq,u)  D De LH
    ou:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                        @@@@@                         #"<<endl; //#
cout <<"  #                                        @@@@@                         #"<<endl; //#7
cout <<"  #                                        @@@@@                         #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ou";

if (ch7==119) // up
goto oy;
if (ch7==100) // right
goto qpu;
if (ch7==115) // down
goto oi;
if (ch7==97)  // left
goto iu;

goto ou;
}
{ // HUMAN KINGDOM 94   (10,7)  PLAINS      (qq,u)  D De LH
    qpu:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             @@@@@                    #"<<endl; //#
cout <<"  #                                             @@@@@                    #"<<endl; //#7
cout <<"  #                                             @@@@@                    #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qpu";

if (ch7==119) // up
goto qpy;
if (ch7==100) // right
goto qqu;
if (ch7==115) // down
goto qpi;
if (ch7==97)  // left
goto ou;

goto qpu;
}
{ // HUMAN KINGDOM 95   (11,7)  PLAINS      (qq,u)  D De LH
    qqu:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                  @@@@@               #"<<endl; //#
cout <<"  #                                                  @@@@@               #"<<endl; //#7
cout <<"  #                                                  @@@@@               #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qqu";

if (ch7==119) // up
goto qqy;
if (ch7==100) // right
goto qwu;
if (ch7==115) // down
goto qqi;
if (ch7==97)  // left
goto qpu;

goto qqu;
}
{ // HUMAN KINGDOM 96   (12,7)  PLAINS      (qq,u)  D De LH
    qwu:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                       @@@@@          #"<<endl; //#
cout <<"  #                                                       @@@@@          #"<<endl; //#7
cout <<"  #                                                       @@@@@          #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qwu";

if (ch7==119) // up
goto qwy;
if (ch7==100) // right
goto qeu;
if (ch7==115) // down
goto qwi;
if (ch7==97)  // left
goto qqu;

goto qwu;
}
{ // HUMAN KINGDOM 97   (13,7)  PLAINS      (qq,u)  D De LH
    qeu:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                            @@@@@     #"<<endl; //#
cout <<"  #                                                            @@@@@     #"<<endl; //#7
cout <<"  #                                                            @@@@@     #"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qeu";

if (ch7==119) // up
goto qey;
if (ch7==100) // right
goto qru;
if (ch7==115) // down
goto qei;
if (ch7==97)  // left
goto qwu;

goto qeu;
}
{ // HUMAN KINGDOM 98   (14,7)  PLAINS      (qq,u)  D De LH
    qru:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                 @@@@@#"<<endl; //#
cout <<"  #                                                                 @@@@@#"<<endl; //#7
cout <<"  #                                                                 @@@@@#"<<endl; //#
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  #                                   XXXXX                              #"<<endl; //@8
cout <<"  #                                   XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qru";

if (ch7==119) // up
goto qry;
if (ch7==100) // right
goto qru;
if (ch7==115) // down
goto qri;
if (ch7==97)  // left
goto qeu;

goto qru;
}
{ // HUMAN KINGDOM 99   (1 ,8)  DEMON ALTAR (qq,i)  D De LH
    qi:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #@@@@@                              XXXXX                              #"<<endl; //@
cout <<"  #@@@@@                              XXXXX                              #"<<endl; //@8
cout <<"  #@@@@@                              XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qi";

if (ch7==119) // up
goto qu;
if (ch7==100) // right
goto wi;
if (ch7==115) // down
goto qi;
if (ch7==97)  // left
goto qi;

goto qi;
}
{ // HUMAN KINGDOM 100  (2 ,8)  PLAINS      (qq,i)  D De LH
    wi:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #     @@@@@                         XXXXX                              #"<<endl; //@
cout <<"  #     @@@@@                         XXXXX                              #"<<endl; //@8
cout <<"  #     @@@@@                         XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="wi";

if (ch7==119) // up
goto wu;
if (ch7==100) // right
goto ei;
if (ch7==115) // down
goto wi;
if (ch7==97)  // left
goto qi;

goto wi;
}
{ // HUMAN KINGDOM 101  (3 ,8)  PLAINS      (qq,i)  D De LH
    ei:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #          @@@@@                    XXXXX                              #"<<endl; //@
cout <<"  #          @@@@@                    XXXXX                              #"<<endl; //@8
cout <<"  #          @@@@@                    XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ei";

if (ch7==119) // up
goto eu;
if (ch7==100) // right
goto ri;
if (ch7==115) // down
goto ei;
if (ch7==97)  // left
goto wi;

goto ei;
}
{ // HUMAN KINGDOM 102  (4 ,8)  PLAINS      (qq,i)  D De LH
    ri:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #               @@@@@               XXXXX                              #"<<endl; //@
cout <<"  #               @@@@@               XXXXX                              #"<<endl; //@8
cout <<"  #               @@@@@               XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ri";

if (ch7==119) // up
goto ru;
if (ch7==100) // right
goto ti;
if (ch7==115) // down
goto ri;
if (ch7==97)  // left
goto ei;

goto ri;
}
{ // HUMAN KINGDOM 103  (5 ,8)  PLAINS      (qq,i)  D De LH
    ti:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                    @@@@@          XXXXX                              #"<<endl; //@
cout <<"  #                    @@@@@          XXXXX                              #"<<endl; //@8
cout <<"  #                    @@@@@          XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ti";

if (ch7==119) // up
goto tu;
if (ch7==100) // right
goto yi;
if (ch7==115) // down
goto ti;
if (ch7==97)  // left
goto ri;

goto ti;
}
{ // HUMAN KINGDOM 104  (6 ,8)  PLAINS      (qq,i)  D De LH
    yi:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                         @@@@@     XXXXX                              #"<<endl; //@
cout <<"  #                         @@@@@     XXXXX                              #"<<endl; //@8
cout <<"  #                         @@@@@     XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="yi";

if (ch7==119) // up
goto yu;
if (ch7==100) // right
goto ui;
if (ch7==115) // down
goto yi;
if (ch7==97)  // left
goto ti;

goto yi;
}
{ // HUMAN KINGDOM 105  (7 ,8)  PLAINS      (qq,i)  D De LH
    ui:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                              @@@@@XXXXX                              #"<<endl; //@
cout <<"  #                              @@@@@XXXXX                              #"<<endl; //@8
cout <<"  #                              @@@@@XXXXX                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ui";

if (ch7==119) // up
goto uu;
if (ch7==100) // right
goto ii;
if (ch7==115) // down
goto ui;
if (ch7==97)  // left
goto yi;

goto ui;
}
{ // HUMAN KINGDOM 106  (8 ,8)  DWARF BORDER(qq,i)  D De LH
    ii:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         #  DWARF BORDER #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   @@@@@                              #"<<endl; //@
cout <<"  #                                   @@@@@                              #"<<endl; //@8
cout <<"  #                                   @@@@@                              #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="ii";

if (ch7==119) // up
goto iu;
if (ch7==100) // right
goto oi;
if (ch7==115) // down
goto ii;
if (ch7==97)  // left
goto ui;

goto ii;
}
{ // HUMAN KINGDOM 107  (9 ,8)  PLAINS      (qq,i)  D De LH
    oi:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX@@@@@                         #"<<endl; //@
cout <<"  #                                   XXXXX@@@@@                         #"<<endl; //@8
cout <<"  #                                   XXXXX@@@@@                         #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="oi";

if (ch7==119) // up
goto ou;
if (ch7==100) // right
goto qpi;
if (ch7==115) // down
goto oi;
if (ch7==97)  // left
goto ii;

goto oi;
}
{ // HUMAN KINGDOM 108  (10,8)  PLAINS      (qq,i)  D De LH
    qpi:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX     @@@@@                    #"<<endl; //@
cout <<"  #                                   XXXXX     @@@@@                    #"<<endl; //@8
cout <<"  #                                   XXXXX     @@@@@                    #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qpi";

if (ch7==119) // up
goto qpu;
if (ch7==100) // right
goto qqi;
if (ch7==115) // down
goto qpi;
if (ch7==97)  // left
goto oi;

goto qpi;
}
{ // HUMAN KINGDOM 109  (11,8)  PLAINS      (qq,i)  D De LH
    qqi:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX          @@@@@               #"<<endl; //@
cout <<"  #                                   XXXXX          @@@@@               #"<<endl; //@8
cout <<"  #                                   XXXXX          @@@@@               #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qqi";

if (ch7==119) // up
goto qqu;
if (ch7==100) // right
goto qwi;
if (ch7==115) // down
goto qqi;
if (ch7==97)  // left
goto qpi;

goto qqi;
}
{ // HUMAN KINGDOM 110  (12,8)  PLAINS      (qq,i)  D De LH
    qwi:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX               @@@@@          #"<<endl; //@
cout <<"  #                                   XXXXX               @@@@@          #"<<endl; //@8
cout <<"  #                                   XXXXX               @@@@@          #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qwi";

if (ch7==119) // up
goto qwu;
if (ch7==100) // right
goto qei;
if (ch7==115) // down
goto qwi;
if (ch7==97)  // left
goto qqi;

goto qwi;
}
{ // HUMAN KINGDOM 111  (13,8)  PLAINS      (qq,i)  D De LH
    qei:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                    @@@@@     #"<<endl; //@
cout <<"  #                                   XXXXX                    @@@@@     #"<<endl; //@8
cout <<"  #                                   XXXXX                    @@@@@     #"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qei";

if (ch7==119) // up
goto qeu;
if (ch7==100) // right
goto qri;
if (ch7==115) // down
goto qei;
if (ch7==97)  // left
goto qwi;

goto qei;

ch7=getch();
locationhistory="qqq";
}
{ // HUMAN KINGDOM 112  (14,8)  PLAINS      (qq,i)  D De LH
    qri:
    system("cls");
cout <<"  ########################################################################"<<endl;
cout <<"  #   X-IS A LOCATION         # HUMAN KINGDOM #   @-IS THE PLAYER        #"<<endl;
cout <<"  ########################################################################"<<endl;
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#1
cout <<"  #                              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     #"<<endl; //#
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@2
cout <<"  #                                   XXXXXXXXXXXXXXXXXXXXXXXXX          #"<<endl; //@
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#3
cout <<"  #          XXXXXXXXXX                    XXXXXXXXXXXXXXXXXXXX          #"<<endl; //#
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@4
cout <<"  #          XXXXXXXXXX                                                  #"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#5
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@6
cout <<"  #                                             XXXXX               XXXXX#"<<endl; //@
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                                                      #"<<endl; //#7
cout <<"  #                                                                      #"<<endl; //#
cout <<"  #                                   XXXXX                         @@@@@#"<<endl; //@
cout <<"  #                                   XXXXX                         @@@@@#"<<endl; //@8
cout <<"  #                                   XXXXX                         @@@@@#"<<endl; //@
cout <<"  ########################################################################"<<endl;
//         #####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@#####@@@@@
//           1    2    3    4    5    6    7    8    9    10   11   12   13   14

ch7=getch();
locationhistory="qri";

if (ch7==119) // up
goto qru;
if (ch7==100) // right
goto qri;
if (ch7==115) // down
goto qri;
if (ch7==97)  // left
goto qei;

goto qri;
}
}

nogol:
forest:
hill:
;

}
{ //GAME RANDOM EVENTS          WIP
lh:






;
}
{ //SPECIAL LOCATIONS           WIP
lc:






;
}
{ //EXPERIENCE & LEVEL          WIP
lvl:




;
}
{ //OPTIONS MENU                COMPLETE

options:
system("cls");

    cout << endl;
system ("title GAME OF STATS / OPTIONS / INFDEV.RS");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"             OPTIONS            "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"1.TEXT COLOR                    "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"2.BACKGROUND COLOR              "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"3.BACK                          "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

    ch2 = getch();

if (ch2==49)
    goto textcolor;
else
if (ch2==50)
    goto backcolor;
else
if (ch2==51)
    goto menu;


textcolor:
    system ("cls");
        cout << endl;
system ("title GAME OF STATS / TEXT COLOR / INFDEV.RS");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"          TEXT COLOR            "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"1.RED                           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"2.BLUE                          "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"3.GREEN                         "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"4.YELLOW                        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"5.BLACK                         "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"6.RESET COLOR                   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"7.BACK                          "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

    ch3 = getch();

if (ch3==49)
    {
    c1=1;
    c2=0;
    c3=0;
    c4=0;
    c5=0;
    goto tcolor;
    }
else
if (ch3==50)
    {
    c1=0;
    c2=1;
    c3=0;
    c4=0;
    c5=0;
    goto tcolor;
    }
else
if (ch3==51)
    {
    c1=0;
    c2=0;
    c3=1;
    c4=0;
    c5=0;
    goto tcolor;
    }
else
if (ch3==52)
    {
    c1=0;
    c2=0;
    c3=0;
    c4=1;
    c5=0;
    goto tcolor;
    }
else
if (ch3==53)
    {
    c1=0;
    c2=0;
    c3=0;
    c4=0;
    c5=1;
    goto tcolor;
    }
else
if (ch3==54)
    {
    c1=0;
    c2=0;
    c3=0;
    c4=0;
    c5=1;
    b5=1;
    system ("color F0");
    }

if (ch3==55);
    goto menu;

tcolor:
if ((c1==1)&&(b1==1))
    system("color 4C");
if ((c1==1)&&(b2==1))
    system("color 1C");
if ((c1==1)&&(b3==1))
    system("color 2C");
if ((c1==1)&&(b4==1))
    system("color 6C");
if ((c1==1)&&(b5==1))
    system("color FC"); //
if ((c2==1)&&(b1==1))
    system("color 49");
if ((c2==1)&&(b2==1))
    system("color 19");
if ((c2==1)&&(b3==1))
    system("color 29");
if ((c2==1)&&(b4==1))
    system("color 69");
if ((c2==1)&&(b5==1))
    system("color F9"); //
if ((c3==1)&&(b1==1))
    system("color 4A");
if ((c3==1)&&(b2==1))
    system("color 1A");
if ((c3==1)&&(b3==1))
    system("color 2A");
if ((c3==1)&&(b4==1))
    system("color 6A");
if ((c3==1)&&(b5==1))
    system("color FA"); //
if ((c4==1)&&(b1==1))
    system("color 4E");
if ((c4==1)&&(b2==1))
    system("color 1E");
if ((c4==1)&&(b3==1))
    system("color 2E");
if ((c4==1)&&(b4==1))
    system("color 6E");
if ((c4==1)&&(b5==1))
    system("color FE"); //
if ((c5==1)&&(b1==1))
    system("color 40");
if ((c5==1)&&(b2==1))
    system("color 10");
if ((c5==1)&&(b3==1))
    system("color 20");
if ((c5==1)&&(b4==1))
    system("color 60");
if ((c5==1)&&(b5==1))
    system("color F0"); //
goto menu;

backcolor:
    system ("cls");
            cout << endl;
system ("title GAME OF STATS / BACKGROUND COLOR / INFDEV.RS");
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"       BACKGROUND COLOR         "                                                            <<a<<  endl;
    cout <<"                         "<<a<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<b<<a<<  endl;
    cout <<"                         "<<a<<"1.RED                           "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"2.BLUE                          "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"3.GREEN                         "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"4.YELLOW                        "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"5.WHITE                         "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"6.RESET COLOR                   "                                                            <<a<<  endl;
    cout <<"                         "<<a<<"7.BACK                          "                                                            <<a<<  endl;
    cout <<"                         "<<a<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<c<<a<<  endl;

    ch4 = getch();

    if (ch4==49)
    {
    b1=1;
    b2=0;
    b3=0;
    b4=0;
    b5=0;
    goto bcolor;
    }
else
if (ch4==50)
    {
    b1=0;
    b2=1;
    b3=0;
    b4=0;
    b5=0;
    goto bcolor;
    }
else
if (ch4==51)
    {
    b1=0;
    b2=0;
    b3=1;
    b4=0;
    b5=0;
    goto bcolor;
    }
else
if (ch4==52)
    {
    b1=0;
    b2=0;
    b3=0;
    b4=1;
    b5=0;
    goto bcolor;
    }
else
if (ch4==53)
    {
    b1=0;
    b2=0;
    b3=0;
    b4=0;
    b5=1;
    goto bcolor;
    }
else
if (ch4==54)
    {
    b1=0;
    b2=0;
    b3=0;
    b4=0;
    b5=1;
    c5=1;
    system ("color F0");
    }

if (ch4==55);
    goto menu;

bcolor:
if ((c1==1)&&(b1==1))
    system("color 4C");
if ((c1==1)&&(b2==1))
    system("color 1C");
if ((c1==1)&&(b3==1))
    system("color 2C");
if ((c1==1)&&(b4==1))
    system("color 6C");
if ((c1==1)&&(b5==1))
    system("color FC"); //
if ((c2==1)&&(b1==1))
    system("color 49");
if ((c2==1)&&(b2==1))
    system("color 19");
if ((c2==1)&&(b3==1))
    system("color 29");
if ((c2==1)&&(b4==1))
    system("color 69");
if ((c2==1)&&(b5==1))
    system("color F9"); //
if ((c3==1)&&(b1==1))
    system("color 4A");
if ((c3==1)&&(b2==1))
    system("color 1A");
if ((c3==1)&&(b3==1))
    system("color 2A");
if ((c3==1)&&(b4==1))
    system("color 6A");
if ((c3==1)&&(b5==1))
    system("color FA"); //
if ((c4==1)&&(b1==1))
    system("color 4E");
if ((c4==1)&&(b2==1))
    system("color 1E");
if ((c4==1)&&(b3==1))
    system("color 2E");
if ((c4==1)&&(b4==1))
    system("color 6E");
if ((c4==1)&&(b5==1))
    system("color FE"); //
if ((c5==1)&&(b1==1))
    system("color 40");
if ((c5==1)&&(b2==1))
    system("color 10");
if ((c5==1)&&(b3==1))
    system("color 20");
if ((c5==1)&&(b4==1))
    system("color 60");
if ((c5==1)&&(b5==1))
    system("color F0"); //
goto menu;

}
{ //CREDITS MENU                COMPLETE

    credits:
    system ("cls");

cout << endl;
system ("title GAME OF STATS / CREDITS / INFDEV.RS");
 cout <<"                         "<< "#######################################" << endl;
 cout <<"                         "<< "#    00  000000    00      00  00 00  #" << endl;
 cout <<"                         "<< "#  00    00  00  00  00  00    0000   #" << endl;
 cout <<"                         "<< "#  00    000000  000000  00    000    #" << endl;
 cout <<"                         "<< "#  00    00 00   00  00  00    0000   #" << endl;
 cout <<"                         "<< "#    00  00  00  00  00    00  00 00  #" << endl;
 cout <<"                         "<< "#                                     #" << endl;
 cout <<"                         "<< "#  000000  0000    00    00      00   #" << endl;
 cout <<"                         "<< "#    00    00    00  00  0000  0000   #" << endl;
 cout <<"                         "<< "#    00    0000  000000  00 0000 00   #" << endl;
 cout <<"                         "<< "#    00    00    00  00  00  00  00   #" << endl;
 cout <<"                         "<< "#    00    0000  00  00  00      00   #" << endl;
 cout <<"                         "<< "#######################################" << endl;
 cout <<"                         "<< "#    This game was presented to you   #" << endl;
 cout <<"                         "<< "#            by CRACK TEAM.           #" << endl;
 cout <<"                         "<< "#    With the help of ---------       #" << endl;
 cout <<"                         "<< "#######################################" << endl;
 cout <<"                         "<< "#         CRACK TEAM WEBSITE          #" << endl;
 cout <<"                         "<< "#    alexco12.koding.com/home.html    #" << endl;
 cout <<"                         "<< "#                                     #" << endl;
 cout <<"                         "<< "#######################################" << endl;
 cout <<"                         "<< "#                                     #" << endl;
 cout <<"                         "<< "#    Members of CRACK TEAM            #" << endl;
 cout <<"                         "<< "#                                     #" << endl;
 cout <<"                         "<< "#  Founder:          Alex Copil       #" << endl;
 cout <<"                         "<< "#                                     #" << endl;
 cout <<"                         "<< "#  Programmers:      Alex Copil       #" << endl;
 cout <<"                         "<< "#                    ----------       #" << endl;
 cout <<"                         "<< "#                    ----------       #" << endl;
 cout <<"                         "<< "#                                     #" << endl;
 cout <<"                         "<< "#  Designers:        Alex Copil       #" << endl;
 cout <<"                         "<< "#                    ----------       #" << endl;
 cout <<"                         "<< "#                    ----------       #" << endl;
 cout <<"                         "<< "#                                     #" << endl;
 cout <<"                         "<< "#  Advertisment:     ----------       #" << endl;
 cout <<"                         "<< "#  Ideas:            Dima Filip       #" << endl;
 cout <<"                         "<< "#                    Mihai Popescu    #" << endl;
 cout <<"                         "<< "#                    Alex Copil       #" << endl;
 cout <<"                         "<< "#######################################" << endl;
 cout <<"                         "<< "#    SPONSORED BY:                    #" << endl;
 cout <<"                         "<< "#       HAPPY LOBSTERS YOUTUBE        #" << endl;
 cout <<"                         "<< "#######################################" << endl;
getch();


goto menu;
}
{ //LUCKY 777                   WIP

lotto:

    l1 = (rand()%7)+1;

system ("title GAME OF STATS / LUCKY 777 / INFDEV.RS");
system("cls");

cout <<"Ahh, so you found me..."<<endl<<"I am the devils left hand..."<<endl;
cout <<"I hold the best gambling games"<<endl<<"in the entire world."<<endl;
cout <<endl<<endl<<"Want to play, just say yes or no."<<endl;

l0=getch();

if (l0==121)
goto gamble;
if (l0==110)
goto menu;

gamble:
    system("cls");
cout <<"Tell me 1 number between 1 and 7."<<endl<<endl;

l0 = getch();
if ((l0>57)||(l0<49))

    goto gamble;
else goto gamble2;


gamble2:
if (l0==l1)
cout <<"You got a favor from the DEVIL";
getch();
}
{ //EXIT                        COMPLETE

exit:
system ("exit");
}

return 140697;
}
