// Se citeste un numar real
//Calculati si afisati
//      A=3*x-1 , x<0
//       =2-x   , x>=0
//      B=5*x   , x<0
//       =2-x   , 0<=x<=10
//       =3+x   , x>10
//      C=2-x   , x<0
//       =max x^2 ; 1/x , x>=0
//      D=min x-1 ; x+3 , x<0
//       =max x+1 ; 2*x , x>=0

#include <iostream>

using namespace std;

int main()
{
    float A,B,C,D;
    float x;

    cout << "X="; cin >> x;

//  AICI INCEPA A

            if (x<0)
                A=3*x-1;
            else
                A=2-x;

//  AICI INCEPE B

            if (x<0)
                B=5*x;
            else if ((0<=0)&&(x<=10))
                    B=2-x;
                 else
                    B=3+x;

//  AICI INCEPA C

            if (x<0)
                C=2-x;
            else if ((x*x)>=(1/x))
                C=x*x;
                 else
                C=1/x;

//  AICI INCEPE D

            if ((x<0)&&((x-1)<=(x+3)))
                D=x-1;
            else if ((x-1)>=(x+3))
                    D=x+3;
                else if ((x>=0)&&(x+1)>=(2*x))
                    D=x+1;
                else
                    D=2*x;

cout << "A=" << A << endl;
cout << "B=" << B << endl;
cout << "C=" << C << endl;
cout << "D=" << D << endl;

return 0;
}
