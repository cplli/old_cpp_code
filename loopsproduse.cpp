/*
Se citeste la tastatura un numar NATURAL "n"
Sa se calculezre si sa se afiseze :
a) produsul cifrelor numarului                    (p)
b) suma cifrelor pare                            (sp)
c) suma cifrelor impare                         (si)
d) suma cifrelor divizibile cu 3                (st)
e) produsul cifrelor divizibile cu trei         (pt)
f) suma cifrelor divizibile cu trei sau cu 5    (stc)
g) produsul cifrelor divizibile cu trei sau cu 5(ptc)
*/

#include <iostream>

using namespace std;

unsigned n,r,a;
unsigned p=1,pt=1,ptc=1;
unsigned sp=0,si=0,st=0,stc=0;



int main()
{

cout << "n="; cin >> n;
cout << "CIFRELE LUI n: ";
while (n!=0)
    {
        r=n%10;
        cout << r << ";";

p   = p*r;

if (r%3==0)
pt  = pt*r;

if (r%3||r%5==0)
ptc = ptc*r;

if (r%2==0)
sp  = sp+r;

if (r%2!=0)
si  = si+r;

if (r%3==0)
st  = st+r;

if (r%3||r%5==0)
stc =stc+r;

        n=n/10;
    }
cout << endl << endl;
cout <<"Produsul cifrelor lui 'n' este " << p <<endl;
cout <<"Produsul cifrelor divizibile cu trei ale lui 'n' este " << pt <<endl;
cout <<"Produsul cifrelor divizibile cu trei sau cu cinci ale lui 'n' este " << ptc <<endl;
cout << endl;
cout <<"Suma cifrelor pare lui 'n' este " << sp <<endl;
cout <<"Suma cifrelor impare lui 'n' este " << si <<endl;
cout <<"Suma cifrelor divizibile cu trei ale lui 'n' este " << st <<endl;
cout <<"Suma cifrelor divizibile cu trei sau cu cinci ale lui 'n' este " << stc <<endl;

cin >> a;

    return 0;
}
