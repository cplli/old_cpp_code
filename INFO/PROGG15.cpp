/*
scrieti programul C++ care:

citeste de la tastatura un sir cu max 255 char
format din litere mici si mari
ale alfabetului englez si afiseaza sirul codificat astfel:

daca numarul de vocale (a,e,i,o,u,A,E,I,O,U) este mai mare sau
egal cu numarul de consoane din sir fiecare vocala se va inlocui cu caracterul urmator din alfabetul englez
altfel fiecare consoana se va inlocui cu char precedent din alfabetul engleze
*/

#include <iostream>
#include <string.h>

using namespace std;char s1[256];char vocale[] = {'a','e','i','o','u','A','E','I','O','U'};int voc;int con;int len;int main (){cout << "S1: ";cin >> s1; len = strlen(s1);for (int i = 0 ; i < len ; i++){
if (strchr(vocale,s1[i]) != 0)	voc ++;}con = len - voc;if (voc >= con){	for (int i = 0 ; i < len ; i++){		if (strchr(vocale,s1[i]) != 0)s1[i] = s1[i] + 1;}
}else{for (int i = 0 ; i < len ; i++){if (strchr(vocale,s1[i]) == 0)s1[i] = s1[i] - 1;}}cout << s1;return 0;}
