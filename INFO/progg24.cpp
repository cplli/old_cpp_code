/*
Se citeste la tastatura un numar nat N != 0 maxim 9 cifre
Sa se verifice daca contine cifre consecutive distincte aflate in ordine strict crescatoare de la S-->D
sau aflate in ordine strict descrescatoare de la D-->S

if (true)
    Se afiseaza pe ecran mesajul
    "Da"
else
    "Nu"

*/
#include <iostream>
#include <string.h>
//#include <conio.h>

using namespace std;

char b[10];
int  n;

int main (){

cin >> b;
/*
for (int i = 0 ; i < 9 ; i++){
    b[i] = getch();
    cout << b[i];
2
	if (b[i] == '\n')
		break;
}
*/

n = strlen(b);

//cout << endl << "SIRUL ESTE: " << b;
//cout << endl << "SIRUL ARE: "<< n;

cout << endl;

for (int i = 0 ; i < n ;i++){
    if (b[i] == b[i+1]-1 && b[i+1] == b[i+2]-1){
        cout << "DA";
        return 0;
    }
}

for (int i = n-1 ; i > 0 ;i--){
    if (b[i] == b[i-1]-1 && b[i-1] == b[i-2]-1){
        cout << "DA";
        return 0;
    }
    else {
        cout << "NU";
        break;
    }
}

return 0;}
