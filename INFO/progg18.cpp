/*
Se citeste un sir de cel mult 100 char
Sa se afiseze pe ecran numarul de vocale din sir
apoi numarul de consoane din sir si apoi
sa se inlocuiasca toate vocalele cu char 'a'

obs. sirul se considera ca e format din litere mici

*/
#include <iostream>
#include <string.h>

using namespace std;

int main (){

char v[6] = {'a','e','i','o','u'};
char s[101];
int  sL=0,vC=0;

//cout << v << endl;

cin >> s;
sL = strlen(s);

for (int i = 0 ; i < sL; i++){
    if (strchr(v,s[i]) != 0){
        vC++;
        s[i] = 'a';
    }
}

cout << "VOCALE: " << vC << endl;
cout << "CONSOANE: " << sL-vC << endl;

cout << endl << s;

return 0;}
