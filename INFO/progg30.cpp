/*
Se citesc de la tastatura

2 nr nat,
 A si B ,
 A<=B
si un nr nat X != 0

sa se afiseze pe ecran acele nr nat aflate in intervalul [A,B]
care sunt div cu X, folosind apeluri ale unui sub program ce
verifica div cu X

*/
#include <iostream>

using namespace std;

bool chdiv (int m,int d){
    if (m%d == 0)
        return true;
        else
        return false;
}

int main (){

int a,b,x;

cout << "A:"; cin >> a;
cout << "B:"; cin >> b;
cout << "X:"; cin >> x;

for (int i = b-a;i>0;i--)
    if(chdiv(i,x))
        cout << i << " este div cu " << x << endl;


return 0;}
